<?php

class RemindersController extends BaseController {

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind()
	{
		$this->layout->content = View::make('login.remind');
	}

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
		if(Request::ajax())
		{
			$estadoUsuario = EstadoUsuario::where('valorEstado', '=', 'ACT')->get();

			$rules = array(
				'correo'					=>	'required|email|exists:usuarios,emailUsuario,idEstadoUsuario,' . $estadoUsuario[0]->idEstadoUsuario,
				'recaptcha_response_field'	=>	'required|recaptcha'
			);

			$validador = Validator::make(Input::all(), $rules);

			if($validador->fails()) {
				$errores = $validador->messages();
				return Response::json(array('error' => True, 'mensaje' => $errores->first()));
			}
			$banner = URL::asset('imagenes/banner_mail_conuca.png');
			$vars = ['nombreSistema'=>'CONUCA', 'anio' => date("Y"),'banner_email'=>$banner];

			View::composer(
				'emails.auth.reminder', 
				function($view) use ($vars) 
				{
			    	$view->with(['nombreSistema'  => $vars['nombreSistema']]);
			    	$view->with(['anio' => $vars['anio']]);
			    	$view->with(['banner_email' => $vars['banner_email']]);
			    }
			);

			switch ($response = Password::remind(array('emailUsuario' => Input::get('correo')), function($mensaje)
			{
				$mensaje->subject('Reestablecer Contraseña');
			}))
			{
				case Password::INVALID_USER:
					return Response::json(array('error' => True, 'mensaje' => Lang::get($response)));

				case Password::REMINDER_SENT:
					return Response::json(array('error' => False, 'mensaje' => Lang::get($response)));
			}
		}
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token)) App::abort(404);

		$this->layout->content = View::make('login.reset');
		$this->layout->content->token = $token;
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset($token = null)
	{
		if(Request::ajax())
		{
			if (is_null($token)) return Response::json(array('error' => True, 'mensaje' => 'No se ha recibido un token.'));

			$credentials = array(
				'emailUsuario' 				=>	Input::get('correo'),
				'password'					=>	Input::get('password'),
				'password_confirmation'		=>	Input::get('passwordre'),
				'token'						=>	$token
			);

			$response = Password::reset($credentials, function($user, $password)
			{
				$user->passwordUsuario = Hash::make($password);

				$user->save();
			});

			switch ($response)
			{
				case Password::INVALID_PASSWORD:
				case Password::INVALID_TOKEN:
				case Password::INVALID_USER:
					return Response::json(array('error' => True, 'mensaje' => Lang::get($response)));

				case Password::PASSWORD_RESET:
					return Response::json(array('error' => False, 'mensaje' => 'Su contraseña a sido reestablecida exitosamente.'));
			}
		}
	}
}
