<?php

class CongresoActividadesController  extends BaseController {

	public function getIndex($id)
	{
		$nombreDeCongreso = DB::table('congreso')
					->where('idCongreso',$id)
                    ->first();

        $nombreCongreso = '<< error >>';
        $congresoOwner	= -1;
        $idDetCongreso 	= -1;
        if($nombreDeCongreso != null)
		{
			$nombreCongreso = $nombreDeCongreso->nomCongreso;
			if(Auth::user()->idUsuario == $nombreDeCongreso->idCreador)
				$congresoOwner	= 1;
		}
		//*************************************************************************
		$actividadesXCongreso = DB::table('actividad')
            ->where('idCongreso','=',$id)
            ->get(array('idActividad' , 'titulo','responsableAct','idAula'));
		
		$this->layout->content = View::make('congresos.ConfigurarActividades',array('idCongreso' => $id,'actividadesCongreso' => $actividadesXCongreso,'nombreCongreso' => $nombreCongreso , 'congresoOwner' => $congresoOwner));
	}
	
	public function actualizarData()
	{
		try {
    		if(Request::ajax())
			{
				$action = Input::get('action');
				$idDC 	= Input::get('idDC');
				$error	= false;
				$returnMSG = 'Actualizado';

				switch ($action) {
				    case "ADD":
						$checkExists = DB::table('actividad')
            							->where('idCongreso','=',$idDC)->where('titulo','=',Input::get('actividad'))->count();

									
						//if($checkExists == 0)
            			if(true)
						{
							$objeto = new Actividad;
							$objeto->titulo	= Input::get('actividad');
							$objeto->responsableAct	= Input::get('responsable');
							$objeto->idAgenda	= 1;
							$objeto->agendado	= false;
							$objeto->idCongreso	= $idDC;
							$objeto->save();
							$returnMSG = 'Agregado';
						}else{
							$error	= true;
							$returnMSG = 'Actividad ya esta creada para este congreso';
						}
				        break;
				    case "UPDATE":
				    	$validRecord = Actividad::where("titulo", "=", Input::get('actividad'))
										->where("idActividad", "<>", Input::get('currentId'))
										->where('idCongreso',$idDC)
										->count();
						//if($validRecord > 0)
						if(false)
						{
							$error	= true;
							$returnMSG = 'Actividad ya esta creada, no se puede modificar';
						}else{
					    	$objeto = Actividad::find(Input::get('currentId'));
							$objeto->titulo	= Input::get('actividad');
							$objeto->responsableAct	= Input::get('responsable');
							$objeto->save();
							DB::table('agenda')->where('idAgenda', $objeto->idAgenda)->update(array('titulo' => $objeto->titulo));

						}
				        break;
				    case "DELETE":
				    	$objeto = Actividad::find(Input::get('currentId'));
				    	$tituloRemover = $objeto->titulo;
				    	
				    	$validDelete = DB::table('agenda')
				    					->where('esActividad',1)
							            ->where('idCongreso',$idDC)
							            ->where('titulo',$tituloRemover)
							            ->count();

						if($validDelete == 0){
				        	Actividad::destroy(Input::get('currentId'));
				        	$returnMSG = 'Eliminado';
				        }else{
							$error	= true;
							$returnMSG = 'Actividad agendada, no se puede eliminar';
				        }
				        break;
				}
				if($error)
					return Response::json(array('error' => True, 'mensaje' => $returnMSG ));
				else
					return Response::json(array('error' => False, 'mensaje' => $returnMSG . ' con &eacute;xito, refrescando...'));
			}
		}catch(Exception $e)
		{
			return Response::json(array('error' => True, 'mensaje' => 'Problemas para actualizar: '  . $e->getMessage()));
		}

	}
}