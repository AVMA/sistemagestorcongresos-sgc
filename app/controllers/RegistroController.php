<?php

class RegistroController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::Make('login.registro');
	}

	public function registroUsuario()
	{
		return DB::transaction(function()
		{		
			DB::beginTransaction();
			if(Request::ajax())
			{
				try{
					$rules = array(
						'correo'					=>	'required|email|unique:usuarios,emailUsuario',
						'password'					=>	'required|min:6',
						'nombres'					=>	'required|regex:/^[a-zA-zñéíóúÑÁÉÍÓÚ]{3,3}[a-zA-zñéíóúÑÁÉÍÓÚ ]*$/i', 
						'apellidos'					=>	'required|regex:/^[a-zA-zñéíóúÑÁÉÍÓÚ]{3,3}[a-zA-zñéíóúÑÁÉÍÓÚ ]*$/i',
						'recaptcha_response_field'	=>	'required|recaptcha'
					);

					$validador = Validator::make(Input::all(), $rules);

					if($validador->fails()) {
						$errores = $validador->messages();
						throw new Exception($errores->first());
					}
					else
					{
						$codConfirmar = str_random(60);	
						$estadoUSR=EstadoUsuario::Where('valorEstado','=','CON')->first();
		                $usuario = Usuario::create(array(
		                    'nombreUsuario'         => Input::get('nombres'),
		                    'apelUsuario'             => Input::get('apellidos'),
		                    'passwordUsuario'         => Hash::make(Input::get('password')),
		                    'emailUsuario'            => Input::get('correo'),
		                    'idEstadoUsuario'         => $estadoUSR->idEstadoUsuario,
		                    'fecModUsuario'         => date('Y-m-d H:i:s'),
		                    'codConfirmarUsuario'    => $codConfirmar
		                ));

						if(!$usuario) throw new Exception('Ha ocurrido un error con la DB!');
						$banner = URL::asset('imagenes/banner_mail_conuca.png');

						$sent=Mail::send(
							//'qCorreos', 
							'emails.registroUsuario', 
							array(
								'nombreSistema'	=>	'CONUCA',
								'correo'		=>	Input::get('correo'),
								'nombres'		=>	Input::get('nombres'),
								'apellidos'		=>	Input::get('apellidos'),
								'anio'			=>	date("Y"),
								'banner_email' 	=>	$banner,
								'URLEliminar'	=> 	URL::action(
														'RegistroController@EliminarRegistro',
														array(
															'codConfirmar' => $codConfirmar
														)
													),
								'rolSistema'	=>	'Autor',
								'URLConfirmar'	=> 	URL::action(
														'RegistroController@ConfirmarRegistro',
														array(
															'codConfirmar' => $codConfirmar
														)
													),
							), 
							function($mensaje)
							{
								$mensaje->to(Input::get('correo'), Input::get('nombres') . ' ' . Input::get('apellidos'))
										->subject('CONUCA: Confirmar registro de usuario');
							}
						);
						if($sent==0)
							throw new Exception("con el servidor de correos, por favor intente nuevamente.");
						DB::commit();
						return Response::json(array('error' => False, 'mensaje' => 'Su usuario ha sido creado exitosamente.<br/> Recibira un email para confirmar su usuario.'));
					}
				}catch(Exception $e)
				{
					DB::rollback();
					return Response::json(array('error' => True, 'mensaje' => 'Se ha encontrado un error '.$e->getMessage()));
				}
			}
		});
	}

	public function EliminarRegistro($codConfirmar)
	{
		$afectadas = Usuario::where('codConfirmarUsuario', '=', $codConfirmar)->delete();
		return Redirect::action('LoginController@index');
	}

	public function ConfirmarRegistro($codConfirmar)
	{
		$afectadas = Usuario::where('codConfirmarUsuario', '=', $codConfirmar)->update(array('codConfirmarUsuario' => null, 'idEstadoUsuario' => 1));
		return Redirect::action('LoginController@index');
	}
}