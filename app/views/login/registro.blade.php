@section('content')
<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Nuevo Usuario - CONUCA</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<form role="form" class="form-horizontal">
						<fieldset>
							<legend>Informaci&oacute;n de usuario</legend>
							<div class="alert alert-info fade in">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								- El correo electr&oacute;nico será su usuario de acceso al sistema.<br>
								- Todos los campos marcados con asterisco son obligatorios.<br>
							</div>
							<div class="form-group">
								<label for="correo" class="col-sm-3 control-label">Correo Electr&oacute;nico*:</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="correo" placeholder="Ingrese su correo" />
								</div>
							</div>
							<div class="form-group">
								<label for="password" class="col-sm-3 control-label">Contrase&ntilde;a*:</label>
								<div class="col-sm-9">
									<input type="password" class="form-control" id="password" placeholder="Ingrese su contraseña" />
								</div>
							</div>
							<div class="form-group">
								<label for="password-re" class="col-sm-3 control-label">Confirmar Contrase&ntilde;a*:</label>
								<div class="col-sm-9">
									<input type="password" class="form-control" id="password-re" placeholder="Ingrese nuevamente su contraseña" />
								</div>
							</div>
							
							<legend style="padding-top: 10px">Eres un robot?</legend>
							<label class="col-sm-3 control-label">reCAPTCHA*:</label>
							<div class="col-sm-9">
								{{Form::captcha();}}
							</div>

							<legend style="padding-top: 10px">Informaci&oacute;n personal</legend>
							<div class="form-group">
								<label for="nombres" class="col-sm-3 control-label">Nombres*:</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="nombres" placeholder="Ingrese sus nombres" />
								</div>
							</div>
							<div class="form-group">
								<label for="apellidos" class="col-sm-3 control-label">Apellidos*:</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="apellidos" placeholder="Ingrese sus apellidos" />
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					<button class="btn btn-default"  onClick="location.href='{{{ URL::action('LoginController@index')}}}'" >Regresar al login</button>
					<button class="btn btn-primary" id="crear">Registrarse</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){

		$('#crear').click(function() 
		{
			var correo = $('#correo').val();
			var correoRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var password = $('#password').val();
			var passwordre = $('#password-re').val();
			var nombres = $('#nombres').val().trim();
			var apellidos = $('#apellidos').val().trim();
			var nombresRegex = /^[a-zA-zñéíóúÑÁÉÍÓÚ]{3,3}[a-zA-zñéíóúÑÁÉÍÓÚ ]*$/;
			var recaptcha_response_field = $('#recaptcha_response_field').val();
			var recaptcha_challenge_field = $('#recaptcha_challenge_field').val();
			var errors = false;

			if(!correoRegex.test(correo))
			{
				alertify.error("No ha ingresado un correo válido.");
				$("#correo").parent().parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{
				$("#correo").parent().parent().removeClass('has-error');
			}

			if(password.length < 7)
			{
				alertify.error("La contraseña debe de tener mas de 6 caracteres.");
				$("#password").parent().parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{
				$("#password").parent().parent().removeClass('has-error');
			}

			if(password.localeCompare(passwordre))
			{
				alertify.error("La contraseña de confirmación no coincide.");
				$("#password-re").parent().parent().removeClass('has-error').addClass('has-error');
				errors = true;				
			}
			else
			{
				$("#password-re").parent().parent().removeClass('has-error');				
			}

			if(!nombresRegex.test(nombres))
			{
				alertify.error("El nombre no es válido.");
				$("#nombres").parent().parent().removeClass('has-error').addClass('has-error');
				errors = true;		
			}
			else 
			{
				$("#nombres").parent().parent().removeClass('has-error');
			}
			
			if(!nombresRegex.test(apellidos))
			{
				alertify.error("El apellido no es válido.");
				$("#apellidos").parent().parent().removeClass('has-error').addClass('has-error');
				errors = true;		
			}
			else 
			{
				$("#apellidos").parent().parent().removeClass('has-error');
			}

			if(!errors)
			{
				$.post("registro", {correo: correo, password: password, nombres: nombres, apellidos: apellidos, recaptcha_response_field: recaptcha_response_field, recaptcha_challenge_field: recaptcha_challenge_field}) 
				.done(function(data, status, jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
						$('#recaptcha_reload').click();
						$('#recaptcha_response_field').val('');
					}
					else
					{
						alertify.success(data.mensaje);
						window.setTimeout(function()
						{
							window.location.replace("{{{ URL::action('LoginController@index')}}}");
						},
						2000);
					}
				})
				.fail(function(data, status, jqXHR)
				{
					alertify.error(data.mensaje);
					$('#recaptcha_reload').click();
					$('#recaptcha_response_field').val('');
				});
			}
		});

		$(document).keypress(function(event)
		{
			if(event.which == 13) {
   				$("#crear").click();
   			}
		});
	});
</script>
@stop