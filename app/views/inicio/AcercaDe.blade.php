@section('content')
<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Sistema Gestor de Congresos (SGC)</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<legend>Trabajo de Graduaci&oacute;n - 2014</legend>
					<table class="table">
					    <tbody>
					        <tr class="bg-primary">
					            <td>Desarrollado por:</td>
					        </tr>
					        <tr class="active">
					            <td>Ana Mar&iacute;a Rodr&iacute;guez Hern&aacute;ndez</td>
					        </tr>
					        <tr class="info">
					            <td>Laura Marilin Canjura Rodr&iacute;guez</td>
					        </tr>
					        <tr class="active">
					            <td>Lil Milagro Medina Arriola</td>
					        </tr>
					        <tr class="info">
					            <td>Edwin Alberto Morales Ramos</td>
					        </tr>
					        <tr class="active">
								<td>Antonio Vladimir Martel Avelar</td>
					        </tr>
					        <tr class="info">
					            <td>Juan Carlos Ju&aacute;rez Ruano</td>
					        </tr>
					        <tr class="active">
					            <td>Julio Jos&eacute; Guevara Valladares</td>
					        </tr>
					        <tr class="info">
				            	<td>Samuel Jerem&iacute;as Mundo Orellana</td>
					        </tr>
					    	<tr><td></td></tr>
					        <tr class="bg-primary">
					            <td>Directores del trabajo:</td>
					        </tr>
					        <tr class="active">
					            <td>Dr. Mauricio Pohl</td>
					        </tr>
					        <tr class="info">
					            <td>Ing. Victor Figueroa</td>
					        </tr>
					        <tr><td></td></tr>
					        <tr class="bg-primary">
					            <td>Coordinador de la Carrera de Ciencias de la Computación:</td>
					        </tr>
					        <tr class="active">
					            <td>Lic.Angel Duarte</td>
					        </tr>	
					    </tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Regresar</button>
				</div>
			</div>

		</div>
	</div>
</div>

<script>
		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('HomeController@inicio')}}";
		});
</script>

@stop