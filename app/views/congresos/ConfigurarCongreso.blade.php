@section('content')
<div class="container">
	<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
		<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
			<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
				<div class="panel-title">Configuraci&oacute;n del Congreso: <strong> {{$info_congreso['general'][0]->nomCongreso}}</strong></div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<form role="form" class="form-horizontal" id="formulario" enctype="multipart/form-data" action="{{URL::action('ManejoArchivosController@subirArchivo')}}" method="POST">
							@if(strcmp($info_congreso['general'][0]->comentarioCongreso,"CREA_SL-ELI_RQST-ADM-AS4P")==0)
							<div class="alert alert-danger fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
								<strong>IMPORTANTE:</strong> Este congreso contiene una solicitud para su eliminaci&oacute;n, para cancelarla  cambie el estado del congreso a "ABIERTO".
							</div>
							@endif
							<fieldset>
								<legend>Informaci&oacute;n del congreso</legend>
								<div class="form-group">
									<label for="nomCongreso" class="col-sm-3 control-label">Nombre del congreso*:</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="nomCongreso" value="{{$info_congreso['general'][0]->nomCongreso}}" placeholder="Ingrese el nombre del Congreso"></input>
									</div>
								</div>
								<div class="form-group">
									<label for="acronimo" class="col-sm-3 control-label">Acr&oacute;nimo*:</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="acronimo" value="{{$info_congreso['general'][0]->acronimoCongreso}}" placeholder="Ingrese el acrónimo del congreso"></input>
									</div>
								</div>
								<div class="form-group">
									<label for="sitioWeb" class="col-sm-3 control-label">Sitio Web*:</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="sitioWeb" value="{{$info_congreso['general'][0]->webCongreso}}" placeholder="Sitio web del congreso"></input>
									</div>
								</div>
								<div class="form-group">
									<label for="ciudad" class="col-sm-3 control-label">Ciudad*:</label>
									<div class="col-sm-9">
										<input type="text" onkeypress="return permitirSoloLetras(event,this);" class="form-control" name="ciudad" value="{{$info_congreso['general'][0]->ciudad}}" placeholder="Ingrese el nombre de la ciudad donde se realizara el congreso" ></input>
									</div>
								</div>
								<div class="form-group">
									<label for="pais" class="col-sm-3 control-label">Pa&iacute;s*:</label>
									<div class="col-sm-9">
										<input type="text" onkeypress="return permitirSoloLetras(event,this);" class="form-control" name="pais" value="{{$info_congreso['general'][0]->pais}}" placeholder="Ingrese el nombre de la pais donde se realizara el congreso" ></input>
									</div>
								</div>
								<div class="form-group">
									<label for="pais" class="col-sm-3 control-label">Fecha de Inicio*:</label>
									<div class="col-sm-9">
										<div class='input-group date span2' id='dpIniControl' data-date-format="DD/MM/YYYY">
											<input type='text' class="form-control" id='dpIni' name="fechaInicio" value="{{date('d/m/Y', strtotime($info_congreso['general'][0]->fecIniCongreso))}}"/>
											<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="pais" class="col-sm-3 control-label">Fecha de Fin*:</label>
									<div class="col-sm-9">
										<div class='input-group date span2' id='dpFinControl' data-date-format="DD/MM/YYYY">
											<input type='text' class="form-control" id='dpFin' name="fechaFin" value="{{date('d/m/Y', strtotime($info_congreso['general'][0]->fecFinCongreso))}}"/> 
											<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="col-sm-3 control-label">Email*:</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="email" value="{{$info_congreso['general'][0]->emailOrgCongreso}}" placeholder="Ingrese el nombre de la ciudad donde se realizara el congreso" ></input>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-3"></div>
									<div class="col-sm-9">
										<div class="alert alert-info fade in">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											<strong>Nota:</strong> El tamaño ideal de la imagen es de 600x150 pixeles, caso contrario la imagen ser&aacute; redimensionada autom&aacute;ticamente.
										</div>
									</div>
									<div class="col-sm-12 col-sm-offset-4">
										<div class="file-preview-frame " id="img-vistaprevia">
											<img src="{{URL::asset('banners')}}/{{$info_congreso['general'][0]->banner}}" id="tempholder" class="file-preview-image">
										</div>
									</div>
									<label for="email" class="col-sm-3 control-label">Imagen o Logo*:</label>
									<div class="col-sm-9">
										<input type="file" accept="image/*" name="nombreArchivo"  required="required">
										<input type="hidden" name="tipoArchivo" value="banner"/>
										<input type="hidden" name="idCongreso" value="{{$idCongreso}}"/>
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="col-sm-3 control-label">Fechas Importantes*:</label>
									<div class="col-sm-9">
										<table class="table table-hover table-condensed sortable" id="tablaFechas">
										<thead>
										<tr>
											<th>
												Descripción
											</th>
											<th>
												Inicio
											</th>
											<th>
												Fin
											</th>
											<th class="unsortable">
												Acci&oacute;n
											</th>
										</tr>
										</thead>
										<tbody>
										 @foreach($info_congreso['fechas_importantes'] as $fechas)
										<tr id="{{$fechas->idFechaImportante}}" valorTipoFecha="{{TipoFecha::Find($fechas->idTipoFecha)->valorTipoFecha}}">
											<td value="{{$fechas->idTipoFecha}}">
												{{TipoFecha::Find($fechas->idTipoFecha)->nomTipoFecha}}
											</td>
											<td >
												{{date('d/m/Y', strtotime($fechas->fecInicio))}}
											</td>
											<td>
												{{date('d/m/Y', strtotime($fechas->fecFin))}}
											</td>
											<td >
												<button type="button" class="eliminarFilaDB btn btn-danger fileinput-remove fileinput-remove-button" style="font-size:5px;">
												<i class="glyphicon glyphicon-remove" style="font-size:10px;"></i>
												</button>
											</td>
										</tr>
										 @endforeach
										</tbody>
										</table>
										<div id="fechas-msg" class="alert alert-warning" style="display:{{$info_congreso['fechas_importantes']==null?'block':'none'}}">
											<strong>NOTA: </strong>No se han agregado fechas.
										</div>
										<button type="button" id="nueva-fecha" class="btn btn-success fileinput-remove fileinput-remove-button" style="font-size:5px;">
										<i class="glyphicon glyphicon-plus" style="font-size:10px;"></i>
										</button>
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="col-sm-3 control-label">Estado Congreso*:</label>
									<div class="col-sm-9">
										<input type="checkbox" name="check-estado"  {{$estadoCong=='SNC' || $estadoCong=='ACT'?'checked':''}}>
									</div>
								</div>

							</fieldset>
						</form>
					</div>
				</div>
			</div>
			<div class="panel-footer">
				<div class="row">
					<div class="col-sm-4">
						<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Regresar</button>
						<button type="button" id="guardarTodo" class="btn btn-primary btn-default">Guardar Cambios</button>
					</div>
					<div class="col-sm-3 col-sm-offset-5">
						<button type="button" id="borrarTodo" class="btn btn-danger btn-default">Borrar este congreso</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--Ventana Modal para nueva entrada (Fecha Importante) -->

<div class="modal fade" id="nueva-fecha-importante-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >Nueva Fecha Importante</h4>
      </div>
      <div class="modal-body">
		<div class="container" style="width:450px">
			<div class="row clearfix">
				<div class="alert alert-info fade in">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<strong>NOTA: </strong> Si desea agregar una nueva fecha importante a la lista, pongase en contacto con el <strong><a href="mailto:conuca@uca.edu.sv">Administrador del sistema </a></strong>.
				</div>
				<div class="col-md-4 column">
					<span style="font-weight:bold">Tipo de Fecha: &nbsp; </span>
				</div>
				<div class="col-md-8 column" > 
					<select class="chosen" id="tipofechas" >
						@foreach($info_congreso['tipo_fecha'] as $tipofecha)
							<option  value="{{$tipofecha->idTipoFecha}}|{{$tipofecha->valorTipoFecha}}">{{$tipofecha->nomTipoFecha}}</option>
						 @endforeach
					</select>
				</div>
			</div>
			<br/>
			<div class="row clearfix">
				<div class="col-md-4 column">
					<span style="font-weight:bold">Inicio: &nbsp; </span>
				</div>
				<div class="col-md-8 column">
					<div class='input-group date span2' id='nfIniControl' data-date-format="DD/MM/YYYY">
					    <input type='text' class="form-control" id='nfIni' name="fechaInicio" />
					    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
					</div>
				</div>
			</div>
			<br/>
			<div class="row clearfix">
				<div class="col-md-4 column">
					 <span style="font-weight:bold">Fin: &nbsp; </span>
				</div>
				<div class="col-md-8 column">
					<div class='input-group date span2' id='nfFinControl' data-date-format="DD/MM/YYYY">
		                <input type='text' class="form-control" id='nfFin' name="fechaFin" />
		                <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
		                </span>
		            </div>
				</div>
			</div>
		  </div>
     	</div>

		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			<button type="button" class="btn btn-primary" id="agregarFechaBTN">Agregar</button>
		</div>
    </div>
  </div>
</div>
<script src="{{ URL::asset('js/sortable.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#tipofechas").chosen({disable_search_threshold: 10});
		$('#dpIniControl').datetimepicker({pickTime: false});
		$('#dpFinControl').datetimepicker({pickTime: false});
		$('#nfIniControl').datetimepicker({pickTime: false});
		$('#nfFinControl').datetimepicker({pickTime: false});
		$("[name='check-estado']").bootstrapSwitch({onColor:'success',offColor:'danger',onText:'Abierto',offText:'Cerrado'});
		
		var fechaRegex = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
		var correoRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		var sitioRegex=  new RegExp('^(https?:\\/\\/)?'+ // protocolo
									'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // dominio
									'((\\d{1,3}\\.){3}\\d{1,3}))'+ // ip en formato ipv4
									'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // puerto y ruta
									'(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string (si tiene)
									'(\\#[-a-z\\d_]*)?$','i'); 

		$("[name=nombreArchivo]").fileinput(
		{
			showUpload: false,
			showRemove: false,
			previewFileType: "image",
			browseClass: "btn btn-success",
			browseLabel: " Buscar",
			browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
			removeClass: "btn btn-danger",
			removeLabel: " Eliminar",
			uploadLabel: " Subir",
			removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
			uploadClass: "btn btn-info",
		});

		$("[name=nombreArchivo]").change(function()
		{
			$("[class='close fileinput-remove text-right']").hide();
		});

		$("[name=nombreArchivo]").change(function(){ 
			$( "#img-vistaprevia" ).css("display","none");
		});

		function buscarID(id,array)
		{
			for (var i=0; i<array.length; i++){
				if(array[i]==id) return true;
			}
			return false;
		}

		$("#cancelarTodo").click(function(){
			if({{strcmp($estadoCong,'SNC')==0?1:0}})
				window.location.href="{{URL::action('HomeController@inicio')}}";
			else
				window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
		});

		function configurarEventos()
		{
			//event.preventDefault();
			$('.eliminarFila').unbind('click');
			$(".eliminarFila").click(function()
			{
				$(this).closest('tr').remove();

				if (document.getElementById('tablaFechas').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length==0)
					$( "#fechas-msg" ).css("display","block");
				else
					$( "#fechas-msg" ).css("display","none");	
			});

			function eliminarRegistro(id,tabla,referencia)
			{
				var ref=referencia;
				$.post("{{URL::action('UsuariosCongresosController@eliminarRegistro')}}", {idRegistro: id,nomTabla:tabla,idDetalleCongreso: "{{$info_congreso['general'][0]->idDetalleCongreso}}"})
				.done(function(data)
				{
					if(data.mensaje.indexOf("ERROR") != -1){
						alertify.error(data.mensaje);
					}
					else{
						alertify.success("Registro eliminado.");
						$(ref).closest('tr').remove();
						if (document.getElementById('tablaFechas').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length==0)
							$( "#fechas-msg" ).css("display","block");
						else
							$( "#fechas-msg" ).css("display","none");
					}
				})
				.fail(function(data, status, jqXHR)
				{
					console.log("Server Returned " + status);
					alertify.error("Error: No se pudo eliminar el registro.");
				});
			}

				$('.eliminarFilaDB').unbind('click');
			$(".eliminarFilaDB").click(function()
			{
				var ref=this;
				alertify.confirm("¿Esta seguro que desea eliminar el registro de manera permanente?", function (e) {
					if (e) {
						var table=$(ref).closest('table').attr('id');
						var id=$(ref).closest('tr').children("td:first-child").attr('value');
						eliminarRegistro(id,table,ref);
						
					}else {
						return;
					}
					e=null;
				});
			});
			return false;
		}

		configurarEventos();

		function cambiarClase(tabla)
		{

			var laTabla="#"+tabla+" [class= bg-success] td button";
			$(laTabla).removeClass('eliminarFila').addClass('eliminarFilaDB');

			laTabla="#"+tabla+" [class= bg-success]";
			$(laTabla).removeClass('bg-success');

			configurarEventos();
		}

		$("#agregarFechaBTN").click(function(){
			var nuevaFechaIni=formatDate(new Date($("#nfIni").val().split("/").reverse().join("/"))); 
			var nuevaFechaFin=formatDate(new Date($("#nfFin").val().split("/").reverse().join("/"))); 
			var errorsF=false;

			if(!fechaRegex.test($("#nfIni").val()))
			{
				alertify.error("No ha ingresado una fecha de inicio válida.");
				$("#nfIni").parent().removeClass('has-error').addClass('has-error');
				errorsF = true;
			}
			else
			{
				$("#nfIni").parent().removeClass('has-error');
			}


			if(!fechaRegex.test($("#nfFin").val()))
			{
				alertify.error("No ha ingresado una fecha de fin válida.");
				$("#nfFin").parent().removeClass('has-error').addClass('has-error');
				errorsF = true;
				return;
			}
			else
			{
				$("#nfFin").parent().removeClass('has-error');
			}

			if(nuevaFechaIni > nuevaFechaFin)
			{
				alertify.error("La fecha de fin debe de ser mayor o igual a la fecha de inicio.");
				$("#nfFin").parent().removeClass('has-error').addClass('has-error');
				errorsF = true;	
			}
			else
			{
				$("#nfFin").parent().removeClass('has-error');	
			}

			if(errorsF)return;

			$( "#tablaFechas tbody" ).append( "<tr class='bg-success' data-toggle='tooltip' data-placement='left' title='Nuevo Elemento (Por Procesar)' valorTipoFecha='"+$("#tipofechas").val().split("|")[1]+"'>" +
				"<td class='bg-success' value="+$("#tipofechas").val().split("|")[0] +">" + $("#tipofechas option:selected").text() + "</td>" +
				"<td class='bg-success'>" + $("#nfIni").val() + "</td>" +
				"<td class='bg-success'>" + $("#nfFin").val() + "</td>" +
				"<td class='bg-success'>" + '<button type="button" class="eliminarFila btn btn-danger fileinput-remove fileinput-remove-button" style="font-size:5px;"> '+
							'<i class="glyphicon glyphicon-remove" style="font-size:10px;"></i></button>' +"</td>" +
			"</tr>" );
			configurarEventos();
			alertify.success("Fecha Agregada.");
			$('#nueva-fecha-importante-form').modal('hide');
			$( "#fechas-msg" ).css("display","none");
		});

		$("#nueva-fecha").click(function(){
			var fechasEnLista = new Array();
			var rows = document.getElementById("tablaFechas").rows;
			for(var i = 1, ceiling = rows.length; i < ceiling; i++) {
				fechasEnLista.push(rows[i].cells[0].getAttribute("value"));
			}

			document.getElementById("tipofechas").options.length = 0;
			var select = document.getElementById("tipofechas");

			@foreach($info_congreso['tipo_fecha'] as $tipofecha)
				if (!buscarID({{$tipofecha->idTipoFecha}},fechasEnLista)){
					var option = document.createElement("option");
					option.text = "{{$tipofecha->nomTipoFecha}}";
					option.value = "{{$tipofecha->idTipoFecha}}|{{$tipofecha->valorTipoFecha}}";
					select.add(option);
					$('#tipofechas').trigger("chosen:updated");
				}
			@endforeach

			if(document.getElementById("tipofechas").options.length == 0){
				alertify.error("No hay fechas disponibles para asignar.");
				return;
			}

			$('#nueva-fecha-importante-form').modal('show');

		});

		function formatDate(date1) {
			return date1.getFullYear() + '-' +
			(date1.getMonth() < 9 ? '0' : '') + (date1.getMonth()+1) + '-' +
			(date1.getDate() < 10 ? '0' : '') + date1.getDate()+' 00:00:00';
		}

		function guardarDatosCongreso()
		{
			var idCongreso="{{$info_congreso['general'][0]->idCongreso}}";
			var nombreCongreso=$("[name=nomCongreso]").val();
			var acronimo=$("[name=acronimo]").val();
			var sitioWeb=$("[name=sitioWeb]").val();
			var ciudad=$("[name=ciudad]").val();
			var pais=$("[name=pais]").val();
			var fechaInicio=formatDate(new Date( $("[name=fechaInicio]").val().split("/").reverse().join("/")));
			var fechaFin=formatDate(new Date( $("[name=fechaFin]").val().split("/").reverse().join("/")));
			var email=$("[name=email]").val();
			var cad=document.getElementById("tempholder").src;
			var banner="";
			var estadoCongreso=document.getElementsByName("check-estado")[0].checked?'ACT':'CER';
			var errors=false;
			if(nombreCongreso.length < 3)
			{
				alertify.error("El nombre del congreso es obligatorio y debe tener al menos 3 caracteres.");
				$("[name='nomCongreso']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{
				$("[name='nomCongreso']").parent().removeClass('has-error');
			}

			if(acronimo.length < 3)
			{
				alertify.error("El acrónimo es obligatorio y debe tener al menos 3 caracteres.");
				$("[name='acronimo']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{
				$("[name='acronimo']").parent().removeClass('has-error');
			}

			if(  !sitioRegex.test(sitioWeb))
			{
				alertify.error("El sitio web es inválido.");
				$("[name='sitioWeb']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{
				$("[name='sitioWeb']").parent().removeClass('has-error');
			}

			if(ciudad.length < 1){
				alertify.error("La ciudad es obligatoria.");
				$("[name='ciudad']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}else{
				$("[name='ciudad']").parent().removeClass('has-error');
			}
			
			if(pais.length < 1){
				alertify.error("El país es obligatorio.");
				$("[name='pais']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}else{
				$("[name='pais']").parent().removeClass('has-error');
			}
			if(  fechaInicio == null || !fechaRegex.test($("[name=fechaInicio]").val()))
			{
				alertify.error("No ha ingresado una fecha de inicio válida.");
				$("[name='fechaInicio']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{
				$("[name='fechaInicio']").parent().removeClass('has-error');
			}
			if( fechaFin == null || !fechaRegex.test($("[name=fechaFin]").val()))
			{
				alertify.error("No ha ingresado una fecha de fin válida.");
				$("[name='fechaFin']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{
				$("[name='fechaFin']").parent().removeClass('has-error');
			}

			if(fechaInicio > fechaFin)
			{
				alertify.error("La fecha de fin debe de ser mayor o igual a la fecha de inicio.");
				$("[name='fechaFin']").parent().removeClass('has-error').addClass('has-error');
				errors = true;	
			}
			else
			{
				$("[name='fechaFin']").parent().removeClass('has-error');	
			}

			if (!correoRegex.test(email)){
				alertify.error("La dirección de correo electrónico es inválida.");
				$("[name='email']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}else{
				$("[name='email']").parent().removeClass('has-error');
			}

			var cad=document.getElementById("tempholder").src;
			if(($("#img-vistaprevia" ).is(':visible') && cad.indexOf("convacio.PNG")!=-1) || (!$("#img-vistaprevia" ).is(':visible') && $(".file-caption-name").text()== "")){
				alertify.error("Debe seleccionar una imagen para el congreso.");
				errors=true;
			}
			else{
				$("#input-21").parent().removeClass('has-error');
			}

			var llamadoPonencias=buscarFecha("Llamado a ponencias");
			var confirmAceptacion=buscarFecha("Confirmación de aceptación de ponencias");
			var confirmAceptacionArticulo=buscarFecha("Confirmación de aceptación de Artículos");
			var recepcionDocumentos=buscarFecha("Recepción de presentación y documentos de apoyo");
			var realizacionCongreso=buscarFecha("Realización del Congreso");
			
			if(!llamadoPonencias || !confirmAceptacion || !confirmAceptacionArticulo || !recepcionDocumentos ||!realizacionCongreso){
				alertify.error("Las fechas importantes no han sido configuradas correctamente.<br><br>Debe agregar como mínimo:<br>"+
								"<strong>*Llamado a ponencias<br><br></strong>"+
								"<strong>*Confirmación de aceptación de ponencias<br><br></strong>"+
								"<strong>*Confirmación de aceptación de Artículos<br><br></strong>"+
								"<strong>*Recepción de presentación y documentos de apoyo<br><br></strong>"+
								"<strong>*Realización del Congreso</strong>");
				errors=true;
			}

			if(errors) return;

			if($(".file-caption-name").text()== "")
			{
				banner='nochange';
			}
			else
			{
				banner=$(".file-caption-name").text();
			}
			console.log($(".file-caption-name").text());
			var fechasImportantes = new Array();
			var fechasImportantesIni = new Array();
			var fechasImportantesFin = new Array();
			var rows = document.getElementById("tablaFechas").rows;
			for(var i = 1, ceiling = rows.length; i < ceiling; i++) {
				if(rows[i].getAttribute("class").indexOf("bg-success")!=-1){
					
					var ftempIni=rows[i].cells[1].innerHTML;
					var ftempFin=rows[i].cells[2].innerHTML;
					var fnewIni=formatDate(new Date( ftempIni.split("/").reverse().join("/")));
					var fnewFin=formatDate(new Date( ftempFin.split("/").reverse().join("/")));
					fechasImportantes.push(rows[i].cells[0].getAttribute("value"));
					fechasImportantesIni.push(fnewIni);
					fechasImportantesFin.push(fnewFin);
				}
			}

			if(sitioWeb.indexOf("http://") == -1 && sitioWeb.indexOf("https://") == -1)
				sitioWeb="http://"+sitioWeb;

			$.post("{{URL::action('UsuariosCongresosController@actualizarCongreso')}}",
			 {idCongreso: "{{$info_congreso['general'][0]->idCongreso}}",
			nombreCongreso: nombreCongreso,
			acronimo:acronimo,
			sitioWeb:sitioWeb,
			ciudad:ciudad,
			pais:pais,
			fechaInicio:fechaInicio,
			fechaFin:fechaFin,
			email:email,
			estadoCongreso:estadoCongreso,
			fechasImportantes:JSON.stringify(fechasImportantes),
			fechasImportantesIni:JSON.stringify(fechasImportantesIni),
			fechasImportantesFin:JSON.stringify(fechasImportantesFin)
			})
			.done(function(data)
			{
				alertify.success("La información general del congreso fue configurada correctamente.");
				cambiarClase("tablaFechas");
				if($(".file-caption-name").text()== "")
				{
					banner='nochange';
					window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
				}
				else
				{
					document.forms["formulario"].submit();
				}
				
			})
			.fail(function(data, status, jqXHR)
			{
				alertify.error("Error: "+data.message);
			});
		}
		function buscarFecha(val){
			var rows = document.getElementById("tablaFechas").rows;
			for(var i = 1, ceiling = rows.length; i < ceiling; i++) {
				if(rows[i].getAttribute("valorTipoFecha")==val.trim())return true;
			}
			return false;

		}
		$("#guardarTodo").click(function(){
			guardarDatosCongreso();
		});
		$("#borrarTodo").click(function(){
			alertify.confirm("Se enviará una solicitud al Administrador del sistema para que elimine este congreso. ¿Está seguro que desea continuar con el proceso de eliminación?", function (e) 
			{
				if (e) {
					$.post(
						"{{URL::action('CongresoController@SolicitudEliminarCongreso')}}",
						{idCongreso: "{{$info_congreso['general'][0]->idCongreso}}"}
					)
					.done(function(data)
					{
						if(data.error)
							 alertify.error(data.mensaje);
						else
						{
							alertify.success(data.mensaje);
							window.setTimeout(function()
							{
								window.location.replace("{{URL::action('HomeController@inicio')}}");
							},
							4000);
							
						}
					})
					.fail(function(data, status, jqXHR)
					{
						alertify.error("Error de comunicación con el servidor.");
					});
				}
			});
		});
	});
</script>
@stop