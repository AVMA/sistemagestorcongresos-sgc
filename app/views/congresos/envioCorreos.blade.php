@section('content')
<div class="col-sm-10 col-sm-offset-1" style="margin-top: 30px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Env&iacute;o de correo</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<form role="form" class="form-horizontal" name="formCorreo" >
						<br>
						<div class="alert alert-info fade in">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<strong>Sugerencia</strong>
							<br/><br/>
							<strong>1) </strong> 
							Puede utilizar las siguientes etiquetas para darle formato a su correo: <br>
							<b>[b]</b>'texto'<b>[/b]</b> para poner texto en <b>negrita</b>,<br>
							<b>[i]</b>'texto'<b>[/i]</b> para poner texto en <i>cursiva</i>,<br>
							<b>[u]</b>'texto'<b>[/u]</b> para poner texto <u>subrayado</u>,<br>
							<b>[t]</b>'texto'<b>[/t]</b> para poner texto como t&iacute;tulo.
							<br/><br/>
							<strong>2) </strong> 
							Puede utilizar las siguientes etiquetas para personalizar su mensaje: <br>
							<b>[*PNombre*]</b> para poner el primer nombre,<br>
							<b>[*PApellido*]</b> para poner el primer apellido,<br>
							<b>[*Nombre_Completo*]</b> para poner el nombre completo,<br>
							<b>[*Titulo_Congreso*]</b> para poner el t&iacute;tulo del congreso en que se encuentra,<br>
							<b>[*Acronimo_Congreso*]</b> para poner el acr&oacute;nimo del congreso en que se encuentra,<br>
							<b>[*Titulo_Investigacion*]</b> para poner el t&iacute;tulo de la investigaci&oacute;n (solo cuando los destinatarios son Investigaciones).

						</div>
						<fieldset>
							<legend>Informaci&oacute;n del mensaje</legend>
							<div class="form-group">
								<label for="remitente" class="col-sm-3 control-label">De*:</label>
								<div class="col-sm-9">
									<input type="text" maxlength="100" class="form-control" id="remitente" name="remitente" value="{{$remitente->nombreUsuario}}&nbsp;{{$remitente->apelUsuario}}&nbsp;({{$remitente->emailUsuario}})" />
									<input type="hidden" maxlength="100" class="form-control" id="congreso" name="congreso" value="{{$idCongreso}}" />
								</div>
							</div>
							<div class="form-group">
								<label for="invest" class="col-sm-3 control-label">Para*:</label>
								<div class="col-sm-6">
									<select multiple data-placeholder="Seleccione investigaciones" style="width:100%" class="chosen" id="invest" size="5">
								 		@foreach ($investigaciones as $investigacion)
								 		<option value="{{$investigacion->idUsuario}}_{{$investigacion->idFicha}}"  name="invest">{{$investigacion->tituloPaper}}</option>
								 		@endforeach
									</select>
								</div>
								<div class="col-sm-3">
									<button type="button" id="btnAgregar" class="btn btn-info btn-default">A&ntilde;adir Usuario</button>
								</div>
							</div>
							<div class="form-group">
								<label for="asunto" class="col-sm-3 control-label">Asunto*:</label>
								<div class="col-sm-9">
									<input type="text" maxlength="100" class="form-control" id="asunto" name="asunto" placeholder="Ingrese el asunto del mensaje" />
								</div>
							</div>
							<div class="form-group">
								<label for="cuerpo" class="col-sm-3 control-label">Mensaje*:</label>
								<div class="col-sm-9">
									<textarea class="form-control" id="cuerpo" name="cuerpo" placeholder="Ingrese el cuerpo del mensaje" rows="10" ></textarea>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Regresar</button>
					<button type="button" id="btnEnviar" class="btn btn-primary btn-default">Enviar Mensaje</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Ventana Modal para agregar investigaciones-->
<div class="modal fade" id="agregar-usuarios-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" >Agregar Investigaci&oacute;n</h4>
			</div>
			<div class="modal-body">
				<div class="container" style="width:700px">
					<div class="row clearfix">
						<label for="destinatario" class="col-sm-4 control-label">Agregar Usuario:</label>
						<div class="col-sm-8">
							<select multiple data-placeholder="Seleccione los destinatarios" style="width:100%" class="chosen" id="destinatarios" size="5">
								@foreach ($admin as $adminis)
						 		<option value="{{$adminis->idUsuario}}_no"  name="destinatario">{{$adminis->nombreUsuario}}&nbsp;{{$adminis->apelUsuario}}</option>
						 		@endforeach
						 		@foreach ($usuarios as $usuario)
						 		<option value="{{$usuario->idUsuario}}_no"  name="destinatario">{{$usuario->nombreUsuario}}&nbsp;{{$usuario->apelUsuario}}&nbsp;({{$usuario->emailUsuario}})</option>
						 		@endforeach
							</select>
						</div>
					</div>
					<br/>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary" id="agregarUsuario" data-dismiss="modal">Agregar</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

	$(document).ready(function()
	{
		$("#destinatarios").chosen({max_selected_options: 20});
		$("#invest").chosen({max_selected_options: 100});
		$("#remitente").prop('disabled', true);

		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
		});

		$("#btnAgregar").click(function(){
			$('#agregar-usuarios-form').modal('show');
		});

		$("#agregarUsuario").click(function(){
			obj= $("#destinatarios option:selected");

	  		var select = document.getElementById("invest");
	   		var items=$("#destinatarios").val();
	   		for (var i=0; i<items.length; i++) {
	     		var option = document.createElement("option");
	     		option.text = $('#destinatarios').children('option[value="'+items[i]+'"]').text();
	     		option.value = items[i];
	     		option.selected = true;
	     		select.add(option);
	     		$("#invest").trigger("chosen:updated");
			};
			$('#destinatarios').prop('selectedIndex',0);
		});

		$("#btnEnviar").click(function()
		{

			var remitente = $("[name=remitente]").val();
			var congreso = $("[name=congreso]").val();
			var destinatarios = $("#invest").val() || [];
			var asunto = $("[name=asunto]").val();
			var cuerpo = $("[name=cuerpo]").val();
			var error = false;

			if( remitente.length == 0 )
			{
				alertify.error("El mensaje debe poseer al menos un remitente.");
				$("#remitente").parent().removeClass('has-error').addClass('has-error');
				error = true;
				var btn=this;
				btn.innerHTML='Enviar Solicitud';
				btn.disabled=false;
			}
			else
			{
				$("#remitente").parent().removeClass('has-error');
			}


			if( remitente.length > 100 )
			{
				alertify.error("El remitente es inválido. Por favor cierre sesión y vuelva a iniciar.");
				$("#remitente").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}
			else
			{
				$("#remitente").parent().removeClass('has-error');
			}

			if( destinatarios.length == 0 )
			{
				alertify.error("El mensaje debe poseer al menos un destinatario.");
				$("#invest").parent().removeClass('has-error').addClass('has-error');
				error = true;
				var btn=this;
				btn.innerHTML='Enviar Solicitud';
				btn.disabled=false;
			}
			else
			{
				$("#invest").parent().removeClass('has-error');
			}


			if( destinatarios.length > 100 )
			{
				alertify.error("El mensaje no debe poseer más de 100 destinatarios.");
				$("#invest").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}
			else
			{
				$("#invest").parent().removeClass('has-error');
			}
			if (cuerpo.search("Titulo_Investigacion") != -1) {
				for (var i = 0; i < destinatarios.length; i++) {
					if( destinatarios[i].substring(destinatarios[i].search("_")+1) == "no" )
					{
						error = true;
					}
				};
				if(error == true){
					alertify.error("Para utilizar la etiqueta de título de investigación, es necesario que entre destinatarios solo existan investigaciones.");
					$("#invest").parent().removeClass('has-error').addClass('has-error');
				}else{
					$("#invest").parent().removeClass('has-error');
				}
			};

			if(asunto.length < 1)
			{
				alertify.error("El asunto del mensaje es obligatorio.");
				$("[name='asunto']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{

				$("[name='asunto']").parent().removeClass('has-error');	
			}

			if(asunto.length > 100)
			{
				alertify.error("El asunto del mensaje es demasiado extenso.");
				$("[name='asunto']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{

				$("[name='asunto']").parent().removeClass('has-error');	
			}
			
			if(cuerpo.length < 1){
				alertify.error("El cuerpo del mensaje es obligatoria.");
				$("[name='cuerpo']").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}else{
				$("[name='cuerpo']").parent().removeClass('has-error');
			}

			if(cuerpo.length > 1000 )
			{
				alertify.error("El cuerpo del mensaje es demasiado extenso.");
				$("[name=cuerpo]").parent().removeClass('has-error').addClass('has-error');
				return;
			}
			else
			{
				$("[name=cuerpo]").parent().removeClass('has-error');
			}
			if(!error)
			{

				$.post("{{URL::action('CorreosController@sendMail')}}", {asunto: asunto, destinatarios: destinatarios, cuerpo: cuerpo, remitente: remitente, congreso: congreso})
				.done(function(data, status,jqXHR)
				{
					
					if(data.error)
					{
						alertify.error(data.mensaje);
						
					}
					else
					{
						alertify.success(data.mensaje);

						document.forms["formCorreo"].submit();				
					}
				})
				.fail(function(data, status,jqXHR)
				{
					
					console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor.");
				});
			}
			else{
				
			}
		});
	});
</script>
@stop