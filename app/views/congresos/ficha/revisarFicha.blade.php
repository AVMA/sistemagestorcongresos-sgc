@section('content')
<div id="popUpRef" class="row">
	<div class="col-sm-12">
		<div class="page-header" style="margin-top: 0px !important;">
			<h3>
				Listado de Fichas
				<div class="pull-right">
					<small>
						<select class="filtro">
							<option value="2">Todas</option>
							<option value="0" selected="selected">Pendientes de revisar</option>
							<option value="1">Revisadas</option>
						</select>
					</small>
				</div>
			</h3>
		</div>
		<div class="row" id="borrarFichas">
			@if (count($fichas) == 0)
			<div class="col-sm-4 col-sm-offset-4" style="text-align: center;">
				<h3><span class='label label-info'>No hay Fichas para mostrar</span></h3>
			</div>
			@else
			@foreach ($fichas as $ficha)
			<div>
			<div class="col-sm-4 fichamini">
				<div class="panel panel-default">
					@if (pathinfo($ficha->rutaFicha, PATHINFO_EXTENSION) == "pdf")
					<div class="pdfMini" data-href="{{ URL::action('ArchivoController@servirArchivo', array($idCongreso,$ficha->rutaFicha)) }}" style="min-height: 200px; height: 200px; max-height: 200px; overflow: auto; background-color: grey;"></div>
					@else
					<div style="min-height: 200px; height: 200px; max-height: 200px;">
						<div class="row" style="text-align: center;">
							<div class="col-sm-3" style="margin-left: 37.5%; margin-top: 57.5px;">
								<button type="button" class="btn btn-default btn-lg" disabled="disabled">
									<span class="glyphicon glyphicon-file"></span>
								</button>								
							</div>
							<div class="col-sm-12">
								<span><strong>Archivo:</strong> {{pathinfo($ficha->rutaFicha, PATHINFO_BASENAME)}}</span>
							</div>
							<div class="col-sm-12">
								<span>No se puede visualizar en el navegador.</span>
							</div>
						</div>
					</div>
					@endif
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-12" style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">
								<span><strong>T&iacute;tulo:</strong> {{$ficha->tituloPaper}}</span>
							</div>
							<div class="col-sm-12">
								<span><strong>Entregado el:</strong> {{$ficha->created_at}}</span>
							</div>
							<div class="col-sm-12" style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">
								<span><strong>Autor Encargado:</strong> {{$ficha->nombreUsuario . " " . $ficha->apelUsuario}}</span>
							</div>
							<div class="col-sm-12" style="text-align: center; margin-top: 5px;">
								<div class="btn-group">
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#popUpRevisor{{$ficha->idFicha}}">Revisar</button>
									<button type="button" class="btn btn-default rechazarFicha" data-href="{{URL::action('RevisarFichaController@rechazarFicha', array($idCongreso,$ficha->idFicha))}}">Rechazar Revisi&oacute;n</button>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
			<div class="modal fade fichabig" id="popUpRevisor{{$ficha->idFicha}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
								<span class="sr-only">Close</span>
							</button>
							<h4 class="modal-title" id="myModalLabel"><strong>T&iacute;tulo:</strong> {{$ficha->tituloPaper}}</h4>
						</div>
						<div class="modal-body" style="padding-top 10px;">
							<div class="row">
								<div class="col-sm-7">
									@if (pathinfo($ficha->rutaFicha, PATHINFO_EXTENSION) == "pdf")
										<div class="pdfBig" data-href="{{ URL::action('ArchivoController@servirArchivo', array($idCongreso,$ficha->rutaFicha)) }}" style="overflow-x: auto; overflow-y: auto; text-align: center; background-color: grey; margin-bottom: 20px;"></div>
									@else
									<div class="row" style="text-align: center; min-height: 400px;">
										<div class="col-sm-2 col-sm-offset-5" style="margin-top: 157.5px;">
											<button type="button" class="btn btn-default btn-lg" disabled="disabled">
												<span class="glyphicon glyphicon-file"></span>
											</button>								
										</div>
										<div class="col-sm-12">
											<span><strong>Archivo:</strong> {{pathinfo($ficha->rutaFicha, PATHINFO_BASENAME)}}</span>
										</div>
										<div class="col-sm-12">
											<span>No se puede visualizar en el navegador.</span>
										</div>
										<div class="col-sm-12">
											<span><a href="{{ URL::action('ArchivoController@servirArchivo', array($idCongreso,$ficha->rutaFicha)) }}"><strong>Link de descarga</strong> </a></span>
										</div>
									</div>
									@endif
									<div class="row">
										<div class="col-sm-12">
											<div class="modal-footer" style="text-align: center;">
												<span><a href="{{ URL::action('ArchivoController@servirArchivo', array($idCongreso,$ficha->rutaFicha)) }}"><strong>Link de descarga</strong> </a></span>
											</div>													
										</div>
									</div>
								</div>
								<div class="col-sm-5" style="border-left: 1px solid #e5e5e5;">
									<div class="row">
										<div class="col-sm-12 alto" style="min-height: 335px;">
											<div class="page-header" style="margin-top: 0px !important;">
												<h3>
													Matriz de Evaluación:
												</h3>
											</div>
											<div style="width: 100%; margin-bottom: 15px; overflow-x: hidden; overflow-y: auto;">
												<table class="table table-bordered" style="text-align: left;">
													<thead>
														<tr>
															<th>Criterio:</th>
															<th>Evaluaci&oacute;n:</th>
														</tr>
													</thead>
													@foreach ($criterios as $criterio)
													
													@if ($criterio->idCriterioEvaluacion == $criterio->idCriterioEvaluacionPadre and $criterio->criterioGrupal == 0)
													<tbody>
														<tr>
															<td>
																<div class="row">
																	<div class="col-sm-12">
																		<span>{{$criterio->nombreCriterio}}:</span>
																	</div>
																</div>
															</td>
															<td>
																<div class="row">
																<div class="col-sm-8">
																<input type="text" data-slider-min="1" data-slider-max="{{$criterio->puntajesMax}}" data-slider-step="1" data-slider-value="1" data-slider-labels='{{json_encode($criterio->puntajes)}}' class="sliderEval"/>
																</div>
																</div>
															</td>
														</tr>
													</tbody>
													@endif
													@if ($criterio->idCriterioEvaluacion == $criterio->idCriterioEvaluacionPadre and $criterio->criterioGrupal != 0)
													<tbody style="border-top: 3px solid #ddd; border-bottom: 3px solid #ddd">
														<tr>
															<td colspan="2">
																<span><strong>Grupo: {{$criterio->nombreCriterio}}</strong></span>
															</td>
														</tr>
														@foreach ($criterio->hijos as $hijo)
														<tr>
															<td>
																<div class="row">
																	<div class="col-sm-12">
																		<span>{{$hijo->nombreCriterio}}:</span>
																	</div>
																</div>
															</td>
															<td>
																<div class="row">
																<div class="col-sm-8">
																<input type="text" data-slider-min="1" data-slider-max="{{$hijo->puntajesMax}}" data-slider-step="1" data-slider-value="1" data-slider-labels='{{json_encode($hijo->puntajes)}}' class="sliderEval"/>
																</div>
																</div>
															</td>
														</tr>
														@endforeach
													@endif
													</tbody>
													@endforeach
												</table>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="modal-footer" style="text-align: center;">
												<div class="btn-group">
													<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
													<button type="button" class="btn btn-primary enviarEval" data-href="{{URL::action('RevisarFichaController@evaluarFicha', array($idCongreso,$ficha->idFicha))}}">Enviar Evaluaci&oacute;n</button>
												</div>
											</div>													
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
			@endforeach
			@endif
		</div>
	</div>
	<div style="position: absolute; top -1000; left: -1000; z-index: -1000;" id="altoClone">
	</div>
</div>
<script type="x-tmpl-mustache" id="templateFicha">
<%#fichas%>
<div>
<div class="col-sm-4 fichamini">
	<div class="panel panel-default">
		<%#esPDF%>
		<div class="pdfMini" data-href="<%rutaArchivo%>" style="min-height: 200px; height: 200px; max-height: 200px; overflow: auto; background-color: grey;"></div>
		<%/esPDF%>
		<%^esPDF%>
		<div style="min-height: 200px; height: 200px; max-height: 200px;">
			<div class="row" style="text-align: center;">
				<div class="col-sm-3" style="margin-left: 37.5%; margin-top: 57.5px;">
					<button type="button" class="btn btn-default btn-lg" disabled="disabled">
						<span class="glyphicon glyphicon-file"></span>
					</button>								
				</div>
				<div class="col-sm-12">
					<span><strong>Archivo:</strong> <%nombreArchivo%></span>
				</div>
				<div class="col-sm-12">
					<span>No se puede visualizar en el navegador.</span>
				</div>
			</div>
		</div>
		<%/esPDF%>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-12" style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">
					<span><strong>T&iacute;tulo:</strong> <%tituloPaper%></span>
				</div>
				<div class="col-sm-12">
					<span><strong>Entregado el:</strong> <%created_at%></span>
				</div>
				<div class="col-sm-12" style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">
					<span><strong>Autor Encargado:</strong> <%nombreUsuario%> <%apelUsuario%></span>
				</div>
				<div class="col-sm-12" style="text-align: center; margin-top: 5px;">
					<div class="btn-group">
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#popUpRevisor<%idFicha%>">Revisar</button>
						<button type="button" class="btn btn-default rechazarFicha" <%#cantidadRevs%>disabled="disabled"<%/cantidadRevs%> data-href="<%rechazarUrl%>">Rechazar Revisi&oacute;n</button>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
<div class="modal fade fichabig" id="popUpRevisor<%idFicha%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="myModalLabel"><strong>T&iacute;tulo:</strong> <%tituloPaper%></h4>
			</div>
			<div class="modal-body" style="padding-top 10px;">
				<div class="row">
					<div class="col-sm-7">
						<%#esPDF%>
						<div class="pdfBig" data-href="<%rutaArchivo%>" style="overflow-x: auto; overflow-y: auto; text-align: center; background-color: grey; margin-bottom: 20px;"></div>
						<%/esPDF%>
						<%^esPDF%>
						<div class="row" style="text-align: center; min-height: 400px;">
							<div class="col-sm-2 col-sm-offset-5" style="margin-top: 157.5px;">
								<button type="button" class="btn btn-default btn-lg" disabled="disabled">
									<span class="glyphicon glyphicon-file"></span>
								</button>								
							</div>
							<div class="col-sm-12">
								<span><strong>Archivo:</strong> <%nombreArchivo%></span>
							</div>
							<div class="col-sm-12">
								<span>No se puede visualizar en el navegador.</span>
							</div>
							<div class="col-sm-12">
								<span><a href="<%rutaArchivo%>"><strong>Link de descarga</strong> </a></span>
							</div>
						</div>
						<%/esPDF%>
						<div class="row">
							<div class="col-sm-12">
								<div class="modal-footer" style="text-align: center;">
									<span><a href="<%rutaArchivo%>"><strong>Link de descarga</strong> </a></span>
								</div>													
							</div>
						</div>
					</div>
					<div class="col-sm-5" style="border-left: 1px solid #e5e5e5;">
						<div class="row">
							<div class="col-sm-12 alto" style="min-height: 335px;">
								<div class="page-header" style="margin-top: 0px !important;">
									<h3>
										Matriz de Evaluación:
									</h3>
								</div>
								<div style="width: 100%; margin-bottom: 15px; overflow-x: hidden; overflow-y: auto;">
									<table class="table table-bordered" style="text-align: left;">
										<thead>
											<tr>
												<th>Criterio:</th>
												<th>Evaluaci&oacute;n:</th>
											</tr>
										</thead>
										@foreach ($criterios as $criterio)
										
										@if ($criterio->idCriterioEvaluacion == $criterio->idCriterioEvaluacionPadre and $criterio->criterioGrupal == 0)
										<tbody>
											<tr>
												<td>
													<div class="row">
														<div class="col-sm-12">
															<span>{{$criterio->nombreCriterio}}:</span>
														</div>
													</div>
												</td>
												<td>
													<div class="row">
													<div class="col-sm-8">
													<input type="text" data-slider-min="1" <%#cantidadRevs%>data-slider-enabled="false"<%/cantidadRevs%> data-slider-max="{{$criterio->puntajesMax}}" data-slider-step="1" data-slider-value="1" data-slider-labels='{{json_encode($criterio->puntajes)}}' data-criterio-id="rev<%idRevisionFicha%>criterio{{$criterio->idCriterioEvaluacion}}" class="sliderEval"/>
													</div>
													</div>
												</td>
											</tr>
										</tbody>
										@endif
										@if ($criterio->idCriterioEvaluacion == $criterio->idCriterioEvaluacionPadre and $criterio->criterioGrupal != 0)
										<tbody style="border-top: 3px solid #ddd; border-bottom: 3px solid #ddd">
											<tr>
												<td colspan="2">
													<span><strong>Grupo: {{$criterio->nombreCriterio}}</strong></span>
												</td>
											</tr>
											@foreach ($criterio->hijos as $hijo)
											<tr>
												<td>
													<div class="row">
														<div class="col-sm-12">
															<span>{{$hijo->nombreCriterio}}:</span>
														</div>
													</div>
												</td>
												<td>
													<div class="row">
													<div class="col-sm-8">
													<input type="text" data-slider-min="1" <%#cantidadRevs%>data-slider-enabled="false"<%/cantidadRevs%> data-slider-max="{{$hijo->puntajesMax}}" data-slider-step="1" data-slider-value="1" data-slider-labels='{{json_encode($hijo->puntajes)}}' data-criterio-id="rev<%idRevisionFicha%>criterio{{$hijo->idCriterioEvaluacion}}" class="sliderEval"/>
													</div>
													</div>
												</td>
											</tr>
											@endforeach
										@endif
										</tbody>
										@endforeach
									</table>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="modal-footer" style="text-align: center;">
									<div class="btn-group">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
										<button type="button" class="btn btn-primary enviarEval" <%#cantidadRevs%>disabled="disabled"<%/cantidadRevs%> data-href="<%revisarUrl%>">Enviar Evaluaci&oacute;n</button>
									</div>
								</div>													
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<%/fichas%>
<%^fichas%>
<div class="col-sm-4 col-sm-offset-4" style="text-align: center;">
	<h3><span class='label label-info'>No hay Fichas para mostrar</span></h3>
</div>
<%/fichas%>
</script>
<script type="text/javascript">
	var alto  = 400;
	$(document).ready(function ()
	{
		$('.alto').first().clone().appendTo('#altoClone');
		alto = $('#altoClone').height() + 177;
		$('#altoClone').remove();

		$('.filtro').change(function()
		{
			var filtro = $(this).val();
			$.get(
				"{{URL::action('RevisarFichaController@fichasFiltro', array($idCongreso))}}",
				{filtro: $(this).val()}
			)
			.done(function(data, status,jqXHR) 
			{
				if(data.error) alertify.error(data.mensaje);
				else
				{
					$('#borrarFichas').children().remove();
					$('#borrarFichas').html(Mustache.render($('#templateFicha').html(), data));
					
					$.each(data.fichas, function(index, ficha)
					{
						$.each(ficha.revisiones, function(index, revision)
						{
							$("[data-criterio-id='rev" + revision.idRevisionFicha + 'criterio' + revision.idCriterioEvaluacion + "']").attr('data-slider-value', parseFloat(revision.valorPuntaje));
						});
					});

					$('.enviarEval').click(enviarEval);
					$('.rechazarFicha').click(rechazarFicha);
					$(".sliderEval").slider({
						formater: function(valor)
						{
							var valores = eval($(this.element).attr('data-slider-labels'));
							return valores[valor-1].nomPuntaje;
						}
					});
					$("[id^='popUpRevisor'] .modal-dialog").width($('#popUpRef').width());
					$('.pdfBig').each(pdfBig);
					$('.pdfMini').each(pdfMini);
				}
			})
			.fail(function(data, status,jqXHR)
			{
				console.log("Server Returned " + status);
				alertify.error("Error de comunicación con el servidor.");
			});
		});

		$('.enviarEval').click(enviarEval);
		$('.rechazarFicha').click(rechazarFicha);
		$(".sliderEval").slider({
			formater: function(valor)
			{
				var valores = eval($(this.element).attr('data-slider-labels'));
				return valores[valor-1].nomPuntaje;
			}
		});
		$("[id^='popUpRevisor'] .modal-dialog").width($('#popUpRef').width());
		$('.pdfBig').each(pdfBig);
		$('.pdfMini').each(pdfMini);
	});

function enviarEval () 
{
	var url = $(this).attr('data-href');
	var $that = $(this);
	alertify.confirm("¿Esta seguro que desea enviar la evaluaci&oacute;n de esta ficha?<br/>(Este proceso no se puede revertir)", function (e) {
		if(e)
		{
			var puntajesSelected = [];
			$that.parents('.fichabig').find('.sliderEval').each(function()
			{
				var puntajeCriterio = eval($(this).attr('data-slider-labels'));
				var algo = $(this).slider('getValue')-1;
				puntajesSelected.push(puntajeCriterio[$(this).slider('getValue')-1].idPuntaje);
			});
			$.get(url,{puntajes : puntajesSelected})
			.done(function(data, status,jqXHR) 
			{
				if(data.error) alertify.error(data.mensaje);
				else
				{
					$that.parents('.modal').modal('hide');
					$that.parents('.modal').siblings('.fichamini').remove();
					alertify.success(data.mensaje);
					//TODO: no eliminar si el filtro es todas y deshabilitarlos controles de acuerdo a eso
					//TODO: pintar el algun lugar el estado de las fichas. si ya esta revisada o le falta
				}
			})
			.fail(function(data, status,jqXHR)
			{
				console.log("Server Returned " + status);
				alertify.error("Error de comunicación con el servidor.");
			});
		}
	});
}

function rechazarFicha () 
{
	var url = $(this).attr('data-href');
	var $that = $(this);
	alertify.confirm("¿Esta seguro que desea rechazar la revision de esta ficha?<br/>(Este proceso no se puede revertir)", function (e) {
		if(e)
		{
			$.get(url,{})
			.done(function(data, status,jqXHR) 
			{
				if(data.error) alertify.error(data.mensaje);
				else
				{
					$that.parents('.fichamini').remove();
					alertify.success(data.mensaje);
				}
			})
			.fail(function(data, status,jqXHR)
			{
				console.log("Server Returned " + status);
				alertify.error("Error de comunicación con el servidor.");
			});
		}
	});
}

function pdfBig()
{
	var $that = $(this);
	$that.height(alto);
	PDFJS.getDocument($that.attr('data-href')).then(function(pdf)
	{
		var cantidadPag = pdf.numPages;
		for(var num = 1; num <= cantidadPag; num++)
		{
			pdf.getPage(num).then(function(page)
			{
				var viewportEscalado = page.getViewport(1);
				var canvas = document.createElement('canvas');
				$(canvas)
				.css('padding-top', '10px')
				.css('background-color', 'grey');
				if(num == cantidadPag) $(canvas).css('padding-bottom', '10px');
				var ctx = canvas.getContext('2d');
				var renderContext = {
					canvasContext: ctx,
					viewport: viewportEscalado
				};
				canvas.height = viewportEscalado.height;
				canvas.width = viewportEscalado.width;

				$that.append(canvas);

				page.render(renderContext);
			});
		}
	});
}

function pdfMini ()
{
	var $that = $(this);
	PDFJS.getDocument($that.attr('data-href')).then(function(pdf)
	{
		pdf.getPage(1).then(function(page)
		{
			var anchoDeseado = $that.width() - 17;
			var viewport = page.getViewport(1);
			var escala = anchoDeseado / viewport.width;
			var viewportEscalado = page.getViewport(escala);
			var canvas = document.createElement('canvas');
			var ctx = canvas.getContext('2d');
			var renderContext = {
				canvasContext: ctx,
				viewport: viewportEscalado
			};
			canvas.height = viewportEscalado.height;
			canvas.width = viewportEscalado.width;

			$that.append(canvas);

			page.render(renderContext);
		});
	});
}
</script>
@stop