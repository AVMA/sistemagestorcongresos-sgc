@section('content')
<div class="row">
	<div class="col-sm-7">
		<div class="panel panel-default" style="margin-top: 15px;">
			<div class="panel-body">
				<div class="page-header" style="margin-top: 0px !important;">
					<h3>Fechas Importantes</h3>
				</div>
				<table class="table table-striped">
					<thead>
						<tr>
							<th></th>
							<th>Descripci&oacute;n</th>
							<th>Fecha de Inicio:</th>
							<th>Fecha de Fin:</th>
						</tr>
					</thead>
					<tbody>
						@if (count($fechasImportantes) == 0)
							<tr>
								<td colspan="4">
									<div class="row">
										<div class="col-sm-8 col-sm-offset-2" style="text-align: center;">
											<h3><span class='label label-info'>No hay congresos para mostrar</span></h3>
										</div>
									</div>
								</td>
							</tr>
						@else
							@foreach ($fechasImportantes as $key => $fechaImportante)
								<tr>
									<td>{{$key +1}}</td>
									<td>{{$fechaImportante->nomTipoFecha}}</td>
									<td>{{date('d/m/Y', strtotime($fechaImportante->fecInicio))}}</td>
									<td>{{date('d/m/Y', strtotime($fechaImportante->fecFin))}}</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-sm-5">
		<div class="panel panel-default" style="margin-top: 15px;">
			<div class="panel-body">
				<div class="page-header" style="margin-top: 0px !important;">
					<h3>Informaci&oacute;n del congreso</h3>
				</div>
				@if ($nomRol =='Chair')	
				<ul class="list-group">
					<a class="list-group-item active">CONGRESO: {{$nombreCongreso}} <small><strong>{{$acronimoCongreso}}</strong></small></a>
					<li class="list-group-item" style="text-align:center">
						<ul class="list-group">
							<img src="{{$banner}}" style="height:63px; width:324px"  alt="banners/convacio.png"/>
						</ul>
					</li>
					<a href="#" class="list-group-item active">Usuarios inscritos<span class="badge">{{$personasInscritas}}</span></a>
					<li class="list-group-item">
						<ul class="list-group">
							<a href="#" class="list-group-item">Miembros del comit&eacute;<span class="badge">{{$pcInscritos}}</span></a>
							<a href="#" class="list-group-item">Revisores<span class="badge">{{$revisoresInscritos}}</span></a>
							<a href="#" class="list-group-item">Autores<span class="badge">{{$autoresInscritos}}</span></a>
						</ul>
					</li>
					<a href="#" class="list-group-item active" >Fichas inscritas<span class="badge">{{$fichasInscritas}}</span></a>
					<li class="list-group-item">
						<ul class="list-group">
							<a href="#" class="list-group-item">Fichas Aceptadas<span class="badge">{{$pcInscritos}}</span></a>
							<a href="#" class="list-group-item">Fichas Sin Revisar<span class="badge">{{$revisoresInscritos}}</span></a>
						</ul>
					</li>
				</ul>
				@else
					<a class="list-group-item active">CONGRESO: {{$nombreCongreso}} <small><strong>{{$acronimoCongreso}}</strong></small></a>
					<li class="list-group-item" style="text-align:center">
						<ul class="list-group">
							<img src="{{$banner}}" style="height:63px; width:324px"  alt="banners/convacio.png"/>
						</ul>
					</li>
				@endif
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default" style="margin-top: 15px;">
			<div class="panel-body">
				<div class="page-header" style="margin-top: 0px !important;">
					<h3>Fichas y art&iacute;culos</h3>
				</div>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>id</th>
							<th>Titulo</th>
							<th>Estado Articulo</th>
							<th>Autores</th>
							<th>Resumen</th>
							<th>Art&iacute;culo</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($listaArticulos as $elemento)
								<tr>
									<td>{{$elemento['idFicha']}}</td>
									<td>{{$elemento['tituloPaper']}}</td>
									<td>{{$elemento['nombreEstado']}}
									<td>{{$elemento['autores']}}</td>
									<td><a href="{{URL::action('ArchivoController@servirArchivo',array('idCongreso'=>$idCongreso,'nombreArchivo'=>$elemento['rutaFicha']))}}">Descargar</a></td>
									<td><a href="{{URL::action('ArchivoController@servirArchivo',array('idCongreso'=>$idCongreso,'nombreArchivo'=>$elemento['rutaPaper']))}}"</a>Descargar</td>
								</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop