@section('content')
	<div class="container">
	@if( $congresoOwner > 0 )
		<div class="row clearfix">
			<div class="col-md-12 column">
				<h3 class="text-center">
					Aulas para congreso "{{ $nombreCongreso }}"
				</h3>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-md-6 column">
				<a id="agregarAula"  href="">Agregar Aula</a>
				<br/><br/>
				<table id="tbl_aulastable" name="tbl_aulastable" class="table display">
					<thead>
						<tr>
							<th>Aula</th>
							<th>Tipo</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					@foreach ($aulasCongreso as $objeto)
						<tr class="clickRow">
							<td id="aulaLabel_{{$objeto->idAula}}" visible="false">
								{{$objeto->nomAula}}
							</td>
							<td id="aulaTipo_{{$objeto->idAula}}" visible="false">
								{{$objeto->tipoAula}}
							</td>
							<td><a id="{{$objeto->idAula}}" class="modificar" href='congresoAulas' title="Modificar">Modificar</a></td>
							<td><a id="{{$objeto->idAula}}" class="eliminar"  href='congresoAulas' title="Eliminar">Eliminar</a></td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
			
			<br/>
			
			<div class="col-md-5 column" style="box-shadow: 0 5px 8px gray;">
			
				<h3 id="ACME_LABEL" name="ACME_LABEL"></h3>
				<input type='hidden' name='action' id='action' value=""></input>
				<input type='hidden' name='idRecord' id='idRecord' value=""></input>
				<br/>
				
				<div class="row clearfix">
					<div class="col-md-3 column">
						<label class="col-sm-3 control-label">Aula: &nbsp;</label>
					</div>
					<div class="col-md-6 column">
						<input name="nomAula" id="nomAula" class="form-control" disabled='' type="text" ></input>
					</div>
					<div class="col-md-4 column"></div>
				</div>
				<br/>

				<div class="row clearfix">
					<div class="col-md-3 column">
						<label class="col-sm-3 control-label">Tipo: &nbsp;</label>
					</div>
					<div class="col-md-6 column">
						<input name="tipoAula" id="tipoAula" class="form-control" disabled='' type="text" ></input>
					</div>
					<div class="col-md-4 column"></div>
				</div>
				<br/>
				<div class="row clearfix">
					<div class="col-md-6 column" style="text-aling:center">
						 <button type="button" id="performAction" name="performAction"  disabled=''  class="btn btn-primary btn-default">Guardar</button>
					</div>
				</div>
				<br/>
			</div>
			
		</div>
	@else
		<div class="row clearfix">
			<div class="col-md-12 column">
				<h3 class="text-center text-error">
					No tiene permisos para editar informaci&oacute;n sobre este congreso.
				</h3>
			</div>
		</div>
	@endif

</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('#tbl_aulastable').dataTable( {
	        "language": {
	        	"lengthMenu"	: "Mostrar _MENU_ por p&aacute;gina",
	            "zeroRecords"	: "No hay coincidencias",
	            "info"			: "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
	            "infoEmpty"		: "No hay records disponibles",
	            "infoFiltered"	: "(filtrado de un total de _MAX_ registros)",
	            "sSearch"		: "Buscar:",
	            "sLoadingRecords": "Cargando...",
				"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "&Uacute;ltimo",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
							},
	        }
    	} );

		$("#tbl_aulastable tbody" ).on('click', 'a.eliminar',  function()
		{
			var gotToURL = this.getAttribute('href');
			var recId	 = this.id;
			alertify.confirm("¿Está seguro quiere borrar este record?", function (e) {
				if (e) {
					$.post("{{ URL::action('CongresoAulasController@actualizarData') }}", {action:'DELETE', currentId : recId,idDC : {{$idCongreso}} })
						.done(function(data, status,jqXHR)
						{
							if(data.error)
							{
								alertify.error(data.mensaje);
							}
							else
							{
								alertify.success(data.mensaje);
								window.setTimeout(function()
								{
									window.location.replace("{{URL::action('CongresoAulasController@getIndex',array($idCongreso) )}}");
								},
								900);
								
							}
						})
						.fail(function(data, status,jqXHR)
						{
							console.log("Server Returned " + status);
							alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
						});
				}
				else {return false;}
			
			});
			return false;
		});
		
		$("#tbl_aulastable tbody" ).on('click', 'a.modificar',  function()
		{
			document.getElementById('ACME_LABEL').innerHTML = 'Modificar';
			var objId = this.id;
			
			nombreAct = document.getElementById('aulaLabel_'+objId).innerHTML;
			nombreAct = nombreAct.replace(/\n|\r/g, "").trim();
			document.getElementById('nomAula').value = nombreAct;
			document.getElementById('nomAula').removeAttribute('disabled');
			document.getElementById('nomAula').focus();

			tipoAula = document.getElementById('aulaTipo_'+objId).innerHTML;
			tipoAula = tipoAula.replace(/\n|\r/g, "").trim();
			document.getElementById('tipoAula').value = tipoAula;
			document.getElementById('tipoAula').removeAttribute('disabled');

			$("[name='performAction']").removeAttr('disabled');
			document.getElementById('action').value = 'UPDATE';
			document.getElementById('idRecord').value = objId;
			
			return false;
		});
		
		$("#agregarAula").click(function()
		{
			document.getElementById('ACME_LABEL').innerHTML = 'Agregar';
			document.getElementById('action').value = 'ADD';

			$("[name='performAction']").removeAttr('disabled');
			$("[name='nomAula']").removeAttr('disabled');
			$("[name='nomAula']").val('');
			document.getElementById('nomAula').focus();

			$("[name='tipoAula']").removeAttr('disabled');
			$("[name='tipoAula']").val('');
			return false;
		});
		
		
		$("#performAction").click(function()
		{
			var pnomAula	= document.getElementById('nomAula').value.trim();
			var ptipoAula	= document.getElementById('tipoAula').value.trim();
			var paction		= document.getElementById('action').value.trim();
			var pcurrentId	= document.getElementById('idRecord').value.trim();
			if (checkFieldsEmpty('nomAula','Ingrese un valor para aula'))
				return;
			/*if (checkFieldsEmpty('tipoAula','Ingrese un valor para tipo de aula'))
				return;*/

			$.post("{{ URL::action('CongresoAulasController@actualizarData') }}", {nomAula: pnomAula, tipoAula : ptipoAula, action:paction, currentId:pcurrentId, idDC : {{$idCongreso}} })
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						alertify.success(data.mensaje);
						window.setTimeout(function()
						{
							window.location.replace("{{URL::action('CongresoAulasController@getIndex',array($idCongreso) )}}");
						},
						900);
						
					}
				})
				.fail(function(data, status,jqXHR)
				{
					console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
				});
		});

		function checkFieldsEmpty(idControl,mensajeStop)
		{
			var detener = false;
			var controlHTML	= $("[name='"+idControl+"']").val().trim();
			if(controlHTML == '')
			{
				alertify.error(mensajeStop);
				$("[name='"+idControl+"']").parent().removeClass('has-error').addClass('has-error');
				detener = true;
			}else{$("[name='"+idControl+"']").parent().removeClass('has-error');}
			return detener;
		};
		
	});
</script>

@stop