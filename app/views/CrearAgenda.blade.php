@section('content')

<div id='agendaContent'>

<br>

<div>
<button id="printPDF" title='Exportación de la agenda a un archivo PDF' type="button" class="btn btn-primary">
  <span class="glyphicon glyphicon-list-alt"></span> Exportar PDF
</button>

<button id="printExcel" title='Exportación de la agenda a un archivo de MS Excel' type="button" class="btn btn-primary">
  <span class="glyphicon glyphicon-floppy-disk"></span> Exportar Excel
</button>

<button id="printDiplomas" title='Generación de diplomas para ponentes' type="button" class="btn btn-primary">
  <span class="glyphicon glyphicon-download-alt"></span> Generar Diplomas
</button>

<button id="notifUsers" title='Se envía un correo a todos los autores responsables que han sido agendados. Se solicita una presentación' type="button" class="btn btn-primary">
  <span class="glyphicon glyphicon-envelope"></span> Notificar Ponentes
</button>

<button id="presentaciones" title='Listado de todas las presentaciones asociadas a las ponencias' type="button" class="btn btn-primary">
  <span class="glyphicon glyphicon-list"></span> Presentaciones
</button>

<br><br>

<button id="solarticulos" title='Se envía un correo a todos los autores responsables de fichas para la carga de "artículos"' type="button" class="btn btn-success">
  <span class="glyphicon glyphicon-envelope"></span> Solicitar Artículos
</button>

<button id="solfuentes" title='Se envía un correo a todos los autores responsables de artículos aprobados para la carga de "fuentes" o material de apoyo' type="button" class="btn btn-success">
  <span class="glyphicon glyphicon-envelope"></span> Solicitar Archivos Fuentes
</button>

<button id="archFuentes" title='Listado de todos los "archivos fuentes" de artículos aprobados' type="button" class="btn btn-success">
  <span class="glyphicon glyphicon-file"></span> Archivos Fuentes
</button><input type='hidden' name='loadFlag' id='loadFlag' value="JUSTLOAD"/>

</div>

<br></br>

<div id='wrap'>
<div id='external-events' >
<h4>Presentaciones</h4>
</div>


<br>
<div id="combob" style = "margin-left:10px">	
{{ Form::label('Aulas:  ', '')}} {{ Form::select('Aula', $aulas , Input::old('aula'), array( 'id' => 'aulas')) }} 
&nbsp&nbsp
</div>


<div id='calendar'></div>

<div style='clear:both'></div>


</div>

</div><!-- agendaContent -->


<div class="modal fade" id="notificacionPonentes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" >Usuarios notificados</h4>
			</div>
			<div class="modal-body" style='max-height:500px;overflow-x:hidden;overflow-y: scroll;'>
				<div class="container" style="width:100%">
				<table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th>Autor responsable</th>
							<th>Correo</th>
							<th>Presentaci&oacute;n</th>
							<th>Fecha</th>
						</tr>
					</thead>
					<tbody id='notificacionTablaCuerpo'>
					</tbody>
				</table>
				<br/>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="notificacionPonentesArticulo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" >Usuarios notificados</h4>
			</div>
			<div class="modal-body" style='max-height:500px;overflow-x:hidden;overflow-y: scroll;'>
				<div class="container" style="width:100%">
				<table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th>Autor responsable</th>
							<th>Correo</th>
							<th id='fichaOarticulo'></th>
						</tr>
					</thead>
					<tbody id='notificacionTablaCuerpoArticulo'>
					</tbody>
				</table>
				<br/>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<!-- DIV para ver presentaciones -->
<div class="modal fade" id="verPresentaciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" >Presentaciones</h4>
			</div>
			<div class="modal-body" style='max-height:500px;overflow-x:hidden;overflow-y: scroll;'>
				<div class="container" style="width:100%">
				<table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th>Autor responsable</th>
							<th>Fecha Ponencia</th>
							<th>Presentaci&oacute;n</th>
						</tr>
					</thead>
					<tbody id='presentacionesDetalle'>
					</tbody>
				</table>
				<br/>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<!-- DIV para ver archivos fuentes de papers -->
<div class="modal fade" id="verFuentes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" > Archivos fuentes de art&iacute;culos aprobados</h4>
			</div>
			<div class="modal-body" style='max-height:500px;overflow-x:hidden;overflow-y: scroll;'>
				<div class="container" style="width:100%">
				<table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th>Autor responsable</th>
							<th>T&iacute;tulo</th>
							<th></th>
						</tr>
					</thead>
					<tbody id='archFuentesDetalle'>
					</tbody>
				</table>
				<br/>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<!-- DIV para ver elegir estilo de diploma -->
<div class="modal fade" id="pickDiseno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" > Diseño </h4>
			</div>
			<div class="modal-body">
				<div class="container" style="width:100%">
				<table class="table">
					<tbody>
						<tr>
							<td>
								@if($estilo['1'] == "")
								<label class="control-label">Sin Definir Estilo 1</label></br>
								@else
									<a href="{{URL::action('CrearAgendaController@imprimirDiplomas',array($idDC,'1') )}}" target="_blank"><img src="{{URL::asset('/')}}{{$estilo['1']}}" width="200" height="150"></a>
								@endif
							</td>
							<td>
								@if($estilo['2'] == "")
								<label class="control-label">Sin Definir Estilo 2</label></br>
								@else
									<a href="{{URL::action('CrearAgendaController@imprimirDiplomas',array($idDC,'2') )}}" target="_blank"><img src="{{URL::asset('/')}}{{$estilo['2']}}" width="150" height="200"></a>
								@endif
							</td>
							<td>
								@if($estilo['3'] == "")
								<label class="control-label">Sin Definir Estilo 3</label></br>
								@else
									<a href="{{URL::action('CrearAgendaController@imprimirDiplomas',array($idDC,'3') )}}" target="_blank"><img src="{{URL::asset('/')}}{{$estilo['3']}}" width="200" height="150"></a>
								@endif
							</td>
						</tr>
					</tbody>
				</table>
				<br/>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>


<link href="{{ URL::asset('/css/Print.css') }}" rel="stylesheet" type="text/css">
<style>
element.style {
		border: solid #000000 0px;
		border-top: solid #dddddd 0px;
		padding-top: 10px;
		min-height: 613px;
		}
</style>

 <script type="text/javascript">
 //arodriguez

	$(document).ready(function()
	{


		$("#aulas").addClass('choosen');
		$("#aulas").chosen({disable_search_threshold: 10});

		$( "#printPDF" ).click(function() {
			$.post("{{ URL::action('CrearAgendaController@verificarAgenda', array($idDC) )}}", {fakeparam : "" })
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						window.open("{{URL::action('CrearAgendaController@imprimirAgenda',array($idDC) )}}", "_blank");
					}
				})
				.fail(function(data, status,jqXHR)
				{
					alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
				});
		});
		$( "#printExcel" ).click(function() {
			$.post("{{ URL::action('CrearAgendaController@verificarAgenda', array($idDC) )}}", {fakeparam : "" })
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						window.open("{{URL::action('CrearAgendaController@exportarAgenda',array($idDC) )}}", "_self");
					}
				})
				.fail(function(data, status,jqXHR)
				{
					alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
				});
		});
		$( "#printDiplomas" ).click(function() {
			$.post("{{ URL::action('CrearAgendaController@verificarDiplomas', array($idDC) )}}", {fakeparam : "" })
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						$('#pickDiseno').modal('show');
					}
				})
				.fail(function(data, status,jqXHR)
				{
					alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
				});
		});
		$( "#notifUsers" ).click(function() {
			$.post("{{ URL::action('CrearAgendaController@notificarUsuario', array($idDC) )}}", {fakeparam : "" })
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						$("#notificacionTablaCuerpo").html(data.mensaje);
						$('#notificacionPonentes').modal('show');
					}
				})
				.fail(function(data, status,jqXHR)
				{
					//console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
				});
		});
		$( "#presentaciones" ).click(function() {
			$.post("{{ URL::action('CrearAgendaController@verPresentaciones', array($idDC) )}}", {fakeparam : "" })
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						$("#presentacionesDetalle").html(data.mensaje);
						$('#verPresentaciones').modal('show');
					}
				})
				.fail(function(data, status,jqXHR)
				{
					//console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
				});
		});
		$( "#solarticulos" ).click(function() {
			$.post("{{ URL::action('CrearAgendaController@solicitarArticulo', array($idDC) )}}", {fakeparam : "" })
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						$("#notificacionTablaCuerpoArticulo").html(data.mensaje);
						$("#fichaOarticulo").html('Ficha');
						$('#notificacionPonentesArticulo').modal('show');
					}
				})
				.fail(function(data, status,jqXHR)
				{ alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");});
		});
		$( "#solfuentes" ).click(function() {
			$.post("{{ URL::action('CrearAgendaController@solicitarFuentes', array($idDC) )}}", {fakeparam : "" })
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						$("#notificacionTablaCuerpoArticulo").html(data.mensaje);
						$("#fichaOarticulo").html('Artículo');
						$('#notificacionPonentesArticulo').modal('show');
					}
				})
				.fail(function(data, status,jqXHR)
				{ alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");});
		});
		$( "#archFuentes" ).click(function() {
			$.post("{{ URL::action('CrearAgendaController@verFuentes', array($idDC) )}}", {fakeparam : "" })
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						$("#archFuentesDetalle").html(data.mensaje);
						$('#verFuentes').modal('show');
					}
				})
				.fail(function(data, status,jqXHR)
				{
					//console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
				});
		});

		var eventosAgendados = <?php echo $eventosAgendados;?>;
		var idCongreso = <?php echo $idDC;?>;
		//alert(idCongreso);
		//console.log(eventosAgendados);

		$( "#radioset" ).buttonset();
		/* INICIALIZANDO LOS EVENTOS EXTERNOS
		-----------------------------------------------------------------*/
		//agregando los eventos
		getEventos();

		$('#external-events div.external-event').each(function() {
		
			// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
			// it doesn't need to have a start or end
			var eventObject = {
				title: $.trim($(this).text()) // use the element's text as the event title
			};
			
			// store the Event Object in the DOM element so we can get to it later
			$(this).data('eventObject', eventObject);
			
			// make the event draggable using jQuery UI
			$(this).draggable({
				/*zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag*/
				scroll: false,
		        helper: 'clone',
		        zIndex: 999,
		        revert: true,
		        revertDuration: 0,
		        appendTo: 'body'
			});
		});

		
	
		/* initialize the calendar
		-----------------------------------------------------------------*/
		
		$('#calendar').fullCalendar
		({
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles',
             'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            buttonText:
            {
                  prev:     ' ◄ ',
                  next:     ' ► ',
                  prevYear: ' &lt;&lt; ',
                  nextYear: ' &gt;&gt; ',
                  today:    'Hoy',
                  month:    'Mes',
                  week:     'Semana',
                  day:      'Día'
            },
			header: {
				left: 'prev,next today',
				//center: 'title',
				right: 'agendaWeek,agendaDay'
			},
			columnFormat:{
			    week: 'dddd dd/MM/yyyy',
			    day: ' dddd dd/MM/yyyy'
			},
			slotEventOverlap: false,
			defaultView: 'agendaWeek',
			editable: true,
			droppable: true,
			allDay: false, // this allows things to be dropped onto the calendar !!!
			allDaySlot: false,
			drop: function(date, allDay) 
			{ // this function is called when something is dropped
			
				// retrieve the dropped element's stored Event Object
				$("#loadFlag").val('TRIGGER');
				var originalEventObject = $(this).data('eventObject');
				
				// we need to copy it, so that multiple events don't have a reference to the same object
				var copiedEventObject = $.extend({}, originalEventObject);
				
				// assign it the date that was reported
				copiedEventObject.start = date;
				copiedEventObject.end    = date;//(date.getTime() + 1800000)/1000;
				copiedEventObject.allDay = false;
				
				// render the event on the calendar
				// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
				$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
				
				// is the "remove after drop" checkbox checked?
				if ($('#drop-remove').is(':checked')) {
					// if so, remove the element from the "Draggable Events" list
					$(this).remove();
				}
			},
			
			eventClick: function(event)
			{
				//alert(event.newEventObject);
				alertify.confirm("Seguro desea remover este evento?", function (e) {
				if (e) {
					var idFicIdAgenda = event.newEventObject.split("@");
					var esActividad;
					var idElemento = event._id;
					var idAgenda = 0;
					var idFichaAct, esActividad, enBase;
					//alert(event.newEventObject);
						
					idFichaAct = idFicIdAgenda[0];
					esActividad = idFicIdAgenda[1];
					enBase = idFicIdAgenda[2];
					$("#loadFlag").val('TRIGGER');
					$.ajax({
						    url: "elimAgenda",
						    type: "POST",
						    data: {
						        	ACTION: "delAge",
	    						    IDELEMENTO: idElemento,
						        	ID: idFichaAct,
						        	ESACTIVIDAD: esActividad

						    	  },
						    dataType: "JSON",
						    success: function (data) 
						    {
						       
						        alertify.success("Evento eliminado con &eacute;xito");
						        $('#calendar').fullCalendar('removeEvents',event._id);

						        //agregando los eventos
								getEventos();
								//$('#calendar').events();

						    },
						    error: function (xhr, ajaxOptions, thrownError) {
						        alert(xhr.status);
						        alert(thrownError);
						    }
						    
						});//ajax
				}
				});

			},
			eventResizeStop: function(event, revertFunc)
			{
				$("#loadFlag").val('TRIGGER');
			},

			eventAfterRender: function(event, revertFunc) //eventAfterRender: function(event)
			{
				var iniEvento = $.fullCalendar.formatDate( event.start, 'yyyy-MM-dd HH:mm');
				var finEvento = $.fullCalendar.formatDate( event.end, 'yyyy-MM-dd HH:mm' )
				var idFicIdAgenda = event.newEventObject.split("@");
				//alert(event.title);

				var idAula = $('#aulas').val();
				var titulo = event.title;
				var esUnico;
				var esActividad;
				var enBase;
				var idElemento = event._id;
				var idFicha = idFicIdAgenda[0];
				var esActividad = idFicIdAgenda[1];
				enBase = idFicIdAgenda[2];
				if($("#loadFlag").val() != 'JUSTLOAD')
				{
					$.ajax({
					    url: "guardarAgenda",
					    type: "POST",
					    data: {
					        	ACTION: "insAge",
	    					    IDELEMENTO: idElemento,
					        	IDFICHA: idFicha,
					        	FECINI: iniEvento,
					        	FECFIN: finEvento,
					        	TITULO: titulo,
					        	IDAULA: idAula,
					        	ESUNICO: false,
					        	ESACTIVIDAD: esActividad,
					        	ENBASE:enBase,
					        	IDCONGRESO: "{{$idDC}}"
					    	  },
					 	dataType: "json",
						success: function (data) 
						{
						  	
							//alert(data);
							var output;
							output = data[0].result;
							//alert(data[0].result);
							if(output == "ErS001")
							{
								alertify.error(data[0].mensaje);
								$('#calendar').fullCalendar('removeEvents',event._id);

							}
							else
							{
							        //agregando los eventos
								getEventos();
							}

					    },
						error: function (xhr, ajaxOptions, thrownError) {
						    alert(xhr.status);
						    alert(thrownError);
						 }
						    
					});//ajax
				}
			},//after render
			events: 
			{
			        url: 'getEventosAgendados',
			        type: 'POST',
			        data: {
			            custom_param1: 'something',
			            custom_param2: 'somethingelse'
			        },
			        error: function() {
			            alert('Hubo un error al cargar la agenda!');
			        },
			        color: '#2EFE2E',   // a non-ajax option
			        textColor: 'black' // a non-ajax option
			},

		});
		$('#calendar').fullCalendar('gotoDate', {{$iniCongreso['1']}}, {{$iniCongreso['2']}}, {{$iniCongreso['3']}});
	});
	function getEventos()
	{
		$.ajax({
			//url: '{{URL::action("CrearAgendaController@getEventos")}}', 
		    url: "getEventos",
		    type: "POST",
		    data: {
		        	ACTION: "getEventos",
		        	IDCONGRESO: "{{$idDC}}"
		    	  },
		    dataType: "JSON",
		    success: function (data) 
		    {
		        var divData;

				$('#external-events').html("");
				divData = data.map(function(d) { return d.div}).join('');
				//creando los eventos externos
				$('#external-events').append(divData);

				$('#external-events div.external-event').each(function() 
				{
					//obt el tit i id
					//console.log($.trim($(this).text()));
					var titulo = $.trim($(this).text());
					var arrVal = titulo.split("@");
					var variables = arrVal[1] + "@" + arrVal[2] +"@" + arrVal[3];

//alert(arrVal[1] + "@" + arrVal[2] +"@" + arrVal[3]);
					var eventObject = {
						title: arrVal[0],//$.trim($(this).text())
						newEventObject: variables
						//id,EsAct,EnBase
					};
					
					//guarda el evento en el DOM, para poderlo optener despues
					$(this).data('eventObject', eventObject);
					
					// make the event draggable using jQuery UI
					$(this).draggable({
						zIndex: 999,
						revert: true,      // hace que el evento vuelca
						revertDuration: 0,  //  posision original despues de drag
						scroll: false	//no permite que se scroll la seccion cuando se drag
					});
				
				});

		    },
		    error: function (xhr, ajaxOptions, thrownError) {
		        alert(xhr.status);
		        alert(thrownError);
		     }
		    
		});

	}
		
</script>
<style>
	
	
	body {
		//margin-top: 40px;
		//text-align: center;
		font-size: 14px;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		}
	#agendaContent{text-align: center;}

	#wrap {
		width: 1100px;
		margin: 0 auto;
		}
		
	#external-events {
		float: left;
		width: 15%;
		padding: 0 10px;
		border: 1px solid #ccc;
		background: #eee;
		text-align: left;
		max-height:700px;
		overflow-x:hidden;
		overflow-y: scroll;
		}

		
		
	#external-events h4 {
		font-size: 16px;
		margin-top: 0;
		padding-top: 1em;
		}
		
	.external-event { 
		margin: 10px 0;
		padding: 2px 4px;
		background: #3366CC;
		color: #fff;
		font-size: .85em;
		cursor: pointer;
		}
		
	#external-events p {
		margin: 1.5em 0;
		font-size: 11px;
		color: #666;
		}
		
	#external-events p input {
		margin: 0;
		vertical-align: middle;
		}

	#calendar {
		float: right;
		width: 900px;
		}

</style>



@stop