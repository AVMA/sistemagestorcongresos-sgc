@section('content')

<div class="container">
	<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
		
		
		<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
			<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
				<div class="panel-title">Subida de Archivos Fuentes</strong></div>
			</div>
			@if( $habilitarFuentes )
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
							<form role="form"  id="archFuentes" name="archFuentes" enctype="multipart/form-data" action="{{URL::action('SumissionController@guardarFuentes')}}" method="POST"  class="form-horizontal">
							
							<input type="hidden" name="idCongreso" value="{{$idCongreso}}"/>
							<input type="hidden" name="totalControlesFuentes" id="totalControlesFuentes">

							<div class="alert alert-info fade in">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								Asocie un archivo por cada art&iacute;culo aprobado.
								<br><br>
								{{$subidaFuentes['ayuda']}}
							</div>
							<?php $contadorControles = 1;?>
							@foreach ($fuentes as $objeto)
								<input type="hidden" name="idPaper_{{$contadorControles}}" id="idPaper_{{$contadorControles}}" value="{{$objeto->idPaper}}" >
								<fieldset>
									<h4><label class="control-label">{{$objeto->tituloPaper}}</label></h4>
									<div class="form-group">
										<div class="col-sm-9">
											@if( $objeto->rutaFuentes == "" )
												No tiene ning&uacute;n archivo asociado
											@else
												Ya dispone de un archivo fuente <a href="{{URL::action('ArchivoController@servirArchivo',array('idCongreso'=>$idCongreso,'nombreArchivo'=>$objeto->rutaFuentes))}}" >
												<span class="glyphicon glyphicon-file"></span> Descargar</a>
											@endif
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-9">
											<input name="fileFuente_{{$contadorControles}}" id="fileFuente_{{$contadorControles}}" type="file">
											<?php $contadorControles = $contadorControles + 1;?>
										</div>
									</div>
								</fieldset>
								<legend></legend>
							@endforeach
						</form>
					</div>
				</div>
			</div>
			<div class="panel-footer">
				<div class="row">
					<div class="col-sm-4">
						<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Regresar</button>
						<button type="button" id="btnEnviarFuentes" class="btn btn-primary btn-default">Subir archivos fuentes</button>
					</div>
				</div>
			</div>
			@else
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<strong>No dispone de art&iacute;culos aprobados</strong>
					</div>
				</div>
				<br/>
				<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Regresar</button>
			</div>
			@endif
		</div>
	</div>
</div>
<!--Ventana Modal para enviar solicitud para ser revisor -->

<div class="modal fade" id="nueva-solicitud-revisor-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="padding-top: 12%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >Enviar solicitud para ser revisor del congreso.</h4>
      </div>
      <div class="modal-body">
		<div class="container" style="width:450px">
			<div class="row clearfix">
				<div class="col-md-4 column">
					<span style="font-weight:bold">Elija la Tem&aacute;tica</span>
				</div>
				<div class="col-md-8 column" id="divTematicas" > 
					<select class="chosen" id="listaTematicas" placeholder="Lista de temáticas" >
					</select>
				</div>
			</div>
			<br/>
     	</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" id="agregarSolicitud">Enviar Aplicaci&oacute;n</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal" >Cancelar</button>
		</div>
	</div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$("#listaTematicas").chosen();
	$("#nueva-solicitud-revisor").click(function(){

		$.post("{{URL::action('CongresoController@listarTematicasDisponibles')}}", {idCongreso: "{{$idCongreso}}",idUsuario:"{{Auth::user()->idUsuario}}"})
		.done(function(data)
		{
			if(data.error){
				alertify.error(data.mensaje);
			}
			else{
				console.log(data.htmlVista);
				$("#divTematicas").html(data.htmlvista);
				$("#listaTematicas").chosen();
				$('#listaTematicas').trigger("chosen:updated");
				$('#nueva-solicitud-revisor-form').modal('show');
			}
		})
		.fail(function(data, status, jqXHR)
		{
			alertify.error("Error: No se puede obtener el listado de temáticas.");
		});
	});

	$("#agregarSolicitud").click(function(){
		alertify.confirm("Si envía la solicitud deberá esperar hasta que esta sea gestionada por el Administrador del congreso. <br>¿Está seguro que desea enviarla en este momento?", function (e) 
		{
			if (e) {
				enviarSolicitudRevisor();

			}
		});
	});

	function enviarSolicitudRevisor()
	{
		var idCongreso="{{$idCongreso}}";
		var idTematica="";
		var idPcTematica=$("#listaTematicas").val();
		var usuariosEnTabla = new Array();
		usuariosEnTabla.push("{{Auth::user()->idUsuario}}");
		$.post("{{URL::action('UsuariosCongresosController@actualizarRevisores')}}",
		 {idCongreso: "{{$idCongreso}}",
		idTematica: idTematica,
		idPcTematica:idPcTematica,
		listaUsuarios:JSON.stringify(usuariosEnTabla)
		})
		.done(function(data)
		{
			if(data.mensaje.indexOf("ERROR") != -1){
				alertify.error(data.mensaje);
			}
			else{
				alertify.success("Petición enviada existosamente, redireccionando...");
				window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
			}
		})
		.fail(function(data, status, jqXHR)
		{
			alertify.error("Error en el servidor");
		});
	}

</script>
<script type="text/javascript">

	$(document).ready(function()
	{
		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
		});

		@if( $habilitarFuentes )
            <?php $contadorControles = 1;?>
            @foreach ($fuentes as $objeto)
                $("#fileFuente_{{$contadorControles}}").fileinput(
                {
                    showUpload: false,
                    showRemove: false,
                    previewFileType: "pdf",
                    browseClass: "btn btn-success",
                    browseLabel: " Buscar",
                    browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
                    removeClass: "btn btn-danger",
                    removeLabel: " Eliminar",
                    removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
                    uploadClass: "btn btn-info",
                });
				$("#fileFuente_{{$contadorControles}}").change(function()
				{
					$("[class='close fileinput-remove text-right']").hide();
				});
                <?php $contadorControles = $contadorControles + 1;?>
            @endforeach
            <?php $contadorControles = $contadorControles - 1;?>
            $("#totalControlesFuentes").val("{{$contadorControles}}");
        @endif

        @if( $habilitarFuentes )
	   		$("#btnEnviarFuentes").click(function()
	        {
	            var totalControls = parseInt($("#totalControlesFuentes").val());
	            var alto     = true;
	            var noValido = false;
	            var mensajeUsuario = 'ALTO, ningún archivo fuente elegido para subir';
	            var mensajeUsuario2= '';

	            var ruta     = "";
	            for (var i=1; i<=totalControls; i++) {
	                ruta = $("#fileFuente_"+i).val().replace(/C:\\fakepath\\/i, '');
	                archivoMETADATA = 0;
	                if(ruta != ""){
						alto = false;
						archivoMETADATA = $("#fileFuente_"+i)[0].files[0].size;
	                
	                if(!/({{$subidaFuentes['extensiones']}})$/i.test(ruta)){
	                	noValido = true;
				    	mensajeUsuario2 = 'ALTO, tipo de archivo no v&aacute;lido. Verifique archivos permitidos para este congreso';
				    }
				    
				    if((archivoMETADATA / 1048576) > {{$subidaFuentes['tamanioMax']}}){
	                	noValido = true;
				    	mensajeUsuario2 = 'ALTO, Tama&ntilde;o M&aacute;ximo de archivo excedido';
				    }
					}
	            }
	            if(alto)
	                alertify.error(mensajeUsuario);
	            else
	            {
	            	if(noValido)
	            		alertify.error(mensajeUsuario2);
	            	else{
	            		//Fake ajax .. para dar una sensacion de progreso al usuario.
		            	$('#cargando-img').removeClass('hidden');
						$("#fade").fadeToggle("slow");
	                	document.forms["archFuentes"].submit();
	            	}
	            }

	        });
		@endif

	});
</script>
@stop