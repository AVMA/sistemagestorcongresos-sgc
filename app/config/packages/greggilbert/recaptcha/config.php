<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| API Keys
	|--------------------------------------------------------------------------
	|
	| Set the public and private API keys as provided by reCAPTCHA.
	|
	*/
	'public_key'	=> '6LdRYfMSAAAAAG8F7YXi-kzFg9cOXApwcstT0rEM',
	'private_key'	=> '6LdRYfMSAAAAADIcw25uJlvFudIIUWEXP-2PslEA',
	
	/*
	|--------------------------------------------------------------------------
	| Template
	|--------------------------------------------------------------------------
	|
	| Set a template to use if you don't want to use the standard one.
	|
	*/
	'template'		=> '',
	
	'options' => array(
        'theme' => 'clean',
        'lang' => 'es'
    ),
);