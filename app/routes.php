<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
|--------------------------------------------------------------------------
| Enrutamiento de la aplicacion
|--------------------------------------------------------------------------
|
| La ventana principal de la aplicación es la que se renderiza en 
| HomeController@inicio. Esta se puede acceder estando loggeado o no
| Cuando se trate de acceder a una seccion que necesita login se renderiza
| automaticamente la ventana de LoginController@index. Pra hacer funcionar
| las nuevas rutas se debe dtomar en cuenta lo siguiente:
|
|	1.	En lugar de hacer return View::make(); 
|	   	hagan $this->layout->content = View::make();
|
| 	2.	En las vistas hay que remover el @extends('layouts.master')
|		Ya que esto hace de manera automaticaahora.
|
| Congresos UCA cuenta con (tres)? rutas basicas para todos los 
| mantenimientos.
|
| http://..../inicio :
| Ruta debajo la cual se pondran los mantenimientos que no esten asociados
| a un congreso en especifico. Todas las rutas debajo de esta cargaran la
| barra de usuario. Buscar el Route::group con 'prefix' => 'inicio'.
|
| http://..../login :
| Ruta debajo la cual se pondran las ventanas de login, registro de usuario,
| etc. La cualidad de esta ruta es que la barra se oculta para todas estas 
| ventanas, Buscar el Route::group con 'prefix' => 'login'.
|
| http://..../congreso/{idCongreso} :
| Ruta que es especifica a un congreso. todas las rutas debajo de ella 
| cargaran correctamente la barra del usuariox congreso tomando en cuanto 
| el usuario loggeado. Tambien proporcona la variable $permisos la cual 
| contiene informacion del congreso actualmente ingresado. Buscar el 
| Route::group con 'prefix' => 'congreso'.
|
*/

//Ruta: /
Route::get('/', function()
	{
		return Redirect::action('HomeController@inicio');
	}
);

//Ruta: /inicio
Route::group(array('prefix' => 'inicio'), function(){

	//Ruta: /inicio/home GET
	Route::get('/', 'HomeController@inicio');

	//Ruta: /inicio/home GET
	Route::post('/', 'HomeController@filtrar');

	//Ruta: /inicio/solicitarCongreso GET
	Route::get(
		'SolicitarCongreso',
		array(
			'before' => 'auth',
			'uses' => 'CongresoController@index'
		)
	);

	//Ruta: /inicio/solicitarCongreso POST
	Route::post(
		'SolicitarCongreso',
		array(
			'before' => 'auth',
			'uses' => 'CongresoController@guardarSolicitud'
		)
	);


	//Ruta: /inicio/verificarSiExiste
	Route::post(
		'verificarSiExiste',
		array(
			'before' => 'authJSON',
			'uses' => 'HomeController@verificarExistente'
		)
	);

	//Ruta: /inicio/inscribirUsuario
	Route::post(
		'inscribirUsuario',
		array(
			'before' => 'auth',
			'uses' => 'HomeController@inscribirUsuario'
		)
	);

	//Ruta: /inicio/AcercaDe GET
	Route::get(
		'AcercaDe',
		array(
			'uses' => 'CongresoController@acercade'
		)
	);

	//Ruta agregada para la seleccion de el rol dentro del congreso
	Route::post('RolCongreso', 'HomeController@homeSeteado');


	//Aprobación de Congreso Lista
	Route::get('AprobacionCongresoLista',array('before' => 'auth', 'uses' => 'CongresoController@MostrarSolicitudesCongreso'));

	//Aprobacion de Congreso Detalles
	Route::get('AprobacionCongresoDetalles/{id}',array('before' => 'auth', 'uses' => 'CongresoController@MostrarSolicitudDetalles'));
	Route::post('AprobacionCongresoDetalles',array('before' => 'auth', 'uses' => 'CongresoController@modificarSolicitud'));
	Route::post('eliminarCongreso',array('before' => 'auth', 'uses' => 'CongresoController@eliminarCongreso'));
	Route::post('SolicitudEliminarCongreso',array('before' => 'auth', 'uses' => 'CongresoController@SolicitudEliminarCongreso'));
	Route::post('cambiarEstadoCongreso',array('before' => 'auth', 'uses' => 'CongresoController@cambiarEstadoCongreso'));
	
	
	Route::post('solicitarRevisor',array('before' => 'auth', 'uses' => 'CongresoController@listarTematicasDisponibles'));
	

	//Administracion de Usuarios
	Route::get('adminusuarios', array('before' => 'auth','uses' => 'AdminusuariosController@index'));
	Route::get('crearusuario', array('before' => 'auth','uses' => 'AdminusuariosController@create'));
	Route::post('agregarusuario', array('uses' => 'AdminusuariosController@store'));
	Route::post('eliminarusuario/{id}', array('uses' => 'AdminusuariosController@destroy'));
	Route::get('editarusuario/{id}', array('before' => 'auth','uses' => 'AdminusuariosController@edit'));
	Route::post('actualizarusuario/{id}', array('uses' => 'AdminusuariosController@update'));

	//Areas Investigacion
	Route::get('areasinvestigacion', array('before' => 'auth','uses' => 'AreasInvestigacionController@index'));
	Route::post('agregarareinv', array('uses' => 'AreasInvestigacionController@create'));
	Route::post('actualizararea/{id}', array('uses' => 'AreasInvestigacionController@edit'));

	//Categorias
	Route::get('categorias', array('before' => 'auth','uses' => 'CategoriasController@index'));
	Route::post('agregarcategoria', array('uses' => 'CategoriasController@create'));
	Route::post('actualizarcategoria/{id}', array('uses' => 'CategoriasController@edit'));

	//Extension Documentos
	Route::get('extdocumentos', array('before' => 'auth','uses' => 'ExtDocsController@index'));
	Route::post('agregarextdoc', array('uses' => 'ExtDocsController@create'));
	Route::post('actualizarextdoc/{id}', array('uses' => 'ExtDocsController@edit'));

	//Tematicas generales
	Route::get('tematicas', array('before' => 'auth','uses' => 'TematicaController@index'));
	Route::post('agregartematica', array('uses' => 'TematicaController@create'));
	Route::post('actualizartematica/{id}', array('uses' => 'TematicaController@edit'));

	//Tipos de Fecha
	Route::get('tiposfecha', array('before' => 'auth','uses' => 'TipoFechaController@index'));
	Route::post('agregartipofecha', array('uses' => 'TipoFechaController@create'));
	Route::post('actualizartipofecha/{id}', array('uses' => 'TipoFechaController@edit'));

	//Mensajeria
	Route::get('EnvioCorreos', array('before' => 'auth','uses' => 'CorreosController@adminIndex'));
	Route::post('EnviarCorreo', array('uses' => 'CorreosController@adminSendMail'));
	Route::get('MostrarCorreos', array('before' => 'auth','uses' => 'CorreosController@adminMostrarMail'));
	Route::get('VerCorreo/{ord}', array('before' => 'auth','uses' => 'CorreosController@adminVerMail'));
});

//Ruta: /login
Route::group(array('prefix' => 'login'), function(){
	
	//Ruta: /login/ GET
	Route::get('/',
		array(
			'before' => 'guest',
			'uses' => 'LoginController@index'
		)
	);

	//Ruta: /login/ POST
	Route::post('/',
		array(
			'before' => 'guestJSON',
			'uses' => 'LoginController@hacerLogin'
		)
	);

	//Ruta: /login/ DELETE
	Route::delete('/',
		array(
			'before' => 'authJSON',
			'uses' => 'LoginController@hacerLogout'
		)
	);

	//Ruta: login/registro GET
	Route::get('registro', 
		array(
			'before' 	=> 'guest', 
			'uses' 		=> 'RegistroController@index'
		)
	);
	
	//Ruta: login/registro POST
	Route::post('registro', 
		array(
			'before'	=> 'guestJSON', 
			'uses' 		=> 'RegistroController@registroUsuario'
		)
	);

	//Ruta: login/confirmarUsuario/{codConfirmar} GET
	Route::get('confirmarUsuario/{codConfirmar}', 
		array(
			'before' 	=> 'guest', 
			'uses' 		=> 'RegistroController@ConfirmarRegistro'
		)
	)->where('codConfirmar','^[a-zA-Z0-9]{60,60}$');

	//Ruta: login/confirmarUsuario/{codConfirmar} POST
	Route::get('cancelarUsuario/{codConfirmar}', 
		array(
			'before' 	=> 'guest', 
			'uses'		=> 'RegistroController@EliminarRegistro'
		)
	)->where('codConfirmar','^[a-zA-Z0-9]{60,60}$');

	//Ruta: login/recordar GET
	Route::get('recordar',
		array(
			'before'	=>	'guest',
			'uses'		=>	'RemindersController@getRemind'
		)
	);

	//Ruta: login/recordar POST
	Route::post('recordar',
		array(
			'before'	=> 'guestJSON',
			'uses'		=> 'RemindersController@postRemind'
		)
	);

	//Ruta: login/recordar/{token} GET
	Route::get('recordar/{token}',
		array(
			'before'	=>	'guest',
			'uses'		=>	'RemindersController@getReset'
		)
	)->where('token', '^[a-zA-Z0-9]{40,40}$');

	//Ruta: login/recordar/{token} POST
	Route::post('recordar/{token}',
		array(
			'before'	=> 'guestJSON',
			'uses'		=> 'RemindersController@postReset'
		)
	)->where('token', '^[a-zA-Z0-9]{40,40}$');
});

//Ruta: /congreso
Route::group(array('prefix' => 'congreso'), function(){

	//Ruta: /congreso/{idCongreso}
	Route::group(array('prefix' => '{idCongreso}'), function(){

		//Ruta: /congreso/{idCongreso}/chair
		Route::group(array('prefix' => 'chair'), function(){

			//Ruta: /congreso/{idCongreso}/chair/cambiarRol
			Route::get(
				'cambiarRol',
				array(
					'before' =>	'auth',
					'uses'	 => 'CambiarRolController@chair'
				)
			);

			//Ruta: /congreso/{idCongreso}/chair/evaluacion
			Route::get(
				'evaluacion',
				array(
					'before' => 'auth',
					'uses'   => 'ConfigurarEvaluacionController@configurarEvaluacion'
				)
			);

			Route::post(
				'evaluacion',
				array(
					'before' => 'auth',
					'uses'   => 'ConfigurarEvaluacionController@guardarConfigurarEval'
				)
			);
		});

		//Ruta: /congreso/{idCOngreso}/comite
		Route::group(array('prefix' => 'comite'), function(){

			//Ruta: /congreso/{idCongreso}/comite/cambiarRol
			Route::get(
				'cambiarRol',
				array(
					'before' =>	'auth',
					'uses'	 => 'CambiarRolController@pc'
				)
			);
		});

		//Ruta: /congreso/{idCOngreso}/revisor
		Route::group(array('prefix' => 'revisor'), function(){

			//Ruta: /congreso/{idCongreso}/revisor/cambiarRol
			Route::get(
				'cambiarRol',
				array(
					'before' =>	'auth',
					'uses'	 => 'CambiarRolController@revisor'
				)
			);

			//Ruta: /congreso/{idCongreso}/revisor/fichas
			Route::get(
				'fichas',
				array(
					'before' => 'auth',
					'uses'	 => 'RevisarFichaController@listarFichas'
				)
			);

			//Ruta: /congreso/{idCongreso}/revisor/papers
			Route::get(
				'articulo',
				array(
					'before' => 'auth',
					'uses'	 => 'RevisarPaperController@listarPaper'
				)
			);

			//Ruta: /congreso/{idCongreso}/revisor/fichas/filtro
			Route::get(
				'fichas/filtro',
				array(
					'before' => 'authJSON',
					'uses'	 => 'RevisarFichaController@fichasFiltro'
				)
			)->where('idFicha','^[0-9]{1,}$');

			//Ruta: /congreso/{idCongreso}/revisor/fichas/filtro
			Route::get(
				'articulo/filtro',
				array(
					'before' => 'authJSON',
					'uses'	 => 'RevisarPaperController@paperFiltro'
				)
			)->where('idPaper','^[0-9]{1,}$');

			//Ruta: /congreso/{idCongreso}/revisor/ficha
			Route::group(array('prefix'	=>	'ficha'), function()
			{

				//Ruta: /congreso/{idCongreso}/revisor/ficha/
				Route::get(
					'/',
					array(
						'before' => 'auth',
						'uses'	 => 'RevisarFichaController@listarFicha'
					)
				);

				//Ruta: /congreso/{idCongreso}/revisor/ficha/{idFicha}
				Route::group(array('prefix'	=>	'{idFicha}'), function()
				{

					//Ruta: /congreso/{idCongreso}/revisor/ficha/{idFicha}/rechazar
					Route::get(
						'/rechazar',
						array(
							'before' => 'authJSON',
							'uses'	 => 'RevisarFichaController@rechazarFicha'
						)
					)->where('idFicha','^[0-9]{1,}$');

					//Ruta: /congreso/{idCongreso}/revisor/ficha/{idFicha}/evaluar
					Route::get(
						'/evaluar',
						array(
							'before' => 'authJSON',
							'uses'	 => 'RevisarFichaController@evaluarFicha'
						)
					)->where('idFicha','^[0-9]{1,}$');
				});

			});

			Route::group(array('prefix'	=>	'paper'), function()
			{

				//Ruta: /congreso/{idCongreso}/revisor/paper/
				Route::get(
					'/',
					array(
						'before' => 'auth',
						'uses'	 => 'RevisarPaperController@listarPaper'
					)
				);

				//Ruta: /congreso/{idCongreso}/revisor/paper/{idPaper}
				Route::group(array('prefix'	=>	'{idPaper}'), function()
				{

					//Ruta: /congreso/{idCongreso}/revisor/paper/{idPaper}/rechazar
					Route::get(
						'/rechazar',
						array(
							'before' => 'authJSON',
							'uses'	 => 'RevisarPaperController@rechazarPaper'
						)
					)->where('idPaper','^[0-9]{1,}$');

					//Ruta: /congreso/{idCongreso}/revisor/paper/{idPaper}/evaluar
					Route::get(
						'/evaluar',
						array(
							'before' => 'authJSON',
							'uses'	 => 'RevisarPaperController@evaluarPaper'
						)
					)->where('idPaper','^[0-9]{1,}$');
				});
			});
		});

		//Ruta: /congreso/{idCongreso}/autor
		Route::group(array('prefix' => 'autor'), function(){

			//Ruta: /congreso/{idCongreso}/autor/cambiarRol
			Route::get(
				'cambiarRol',
				array(
					'before' =>	'auth',
					'uses'	 => 'CambiarRolController@autor'
				)
			);

		});

		//Ruta: /congreso/{idCongreso}/
		Route::get(
			'/',
			array(
				'before' => 'auth',
				'uses'	 => 'UsuariosCongresosController@establecerCongreso'
			)
		);

		Route::post(
			'gestionArchivos',
			array(
				'before' => 'auth',
				'uses'	 => 'ManejoArchivosController@subirArchivo'
			)
		);
	
		//Ruta: /congreso/{idCongreso}/archivos/{nombreArchivo}
		Route::get(
			'/archivos/{nombreArchivo}',
			array(
				'before' => 'auth',
				'uses'	 =>	'ArchivoController@servirArchivo'
			)
		)->where("nombreArchivo","^.*$");

		Route::get(
			'/archivosPreview/{nombreArchivo}',
			array(
				'before' => 'auth',
				'uses'	 =>	'ArchivoController@previsualizarArchivo'
			)
		)->where("nombreArchivo","^.*$");
		
		//Ruta: /congreso/{idCongreso}/configurar -- Elementos necesarios para dar de alta  (Activar)
		Route::get(
			'configurar',
			array(
				'before' => 'auth',
				'uses' => 'UsuariosCongresosController@editarCongreso'
			)
		);

		Route::get(
			'configurarPost',
			array(
				'before' => 'auth',
				'uses' => 'UsuariosCongresosController@editarPostCongreso'
			)
		)->where('id','^[0-9]{1,}$');

		Route::get(
			'gestionarRevisores',
			array(
				'before' => 'auth',
				'uses' => 'UsuariosCongresosController@editarRevisoresPorPC'
			)
		)->where('id','^[0-9]{1,}$');

		Route::get(
			'gestionarFichas',
			array(
				'before' => 'auth',
				'uses' => 'UsuariosCongresosController@listarFichas'
			)
		)->where('id','^[0-9]{1,}$');

		Route::get(
			'DetalleFicha/{idFicha}',
			array(
				'before' => 'auth',
				'uses' => 'UsuariosCongresosController@listarDetalleFicha'
			)
		)->where('idFicha','^[0-9]{1,}$');

		Route::get(
			'DetallePaper/{idFicha}',
			array(
				'before' => 'auth',
				'uses' => 'UsuariosCongresosController@listarDetallePaper'
			)
		)->where('idFicha','^[0-9]{1,}$');

		Route::post(
			'aprobarRechazarFP',
			array(
				'before' => 'auth',
				'uses' => 'UsuariosCongresosController@aprobarRechazarFP'
			)
		);

		Route::post(
			'rechazarTodasFP',
			array(
				'before' => 'auth',
				'uses' => 'UsuariosCongresosController@rechazarTodasFP'
			)
		);

		Route::get(
			'gestionarPapers',
			array(
				'before' => 'auth',
				'uses' => 'UsuariosCongresosController@listarPapers'
			)
		)->where('id','^[0-9]{1,}$');

		Route::post(
			'aprobarRevisores',
			array(
				'before' => 'auth',
				'uses' => 'UsuariosCongresosController@aprobarRegistroRevisor'
			)
		);

		Route::get(
			'confirmacionRevisor',
			array(
				'before' => 'auth',
				'uses' => 'UsuariosCongresosController@aceptarRolRevisor'
			)
		);

		Route::get(
			'anularInvitacionRevisor',
			array(
				'before' => 'auth',
				'uses' => 'UsuariosCongresosController@rechazarRolRevisor'
			)
		);

		Route::get(
			'invitarRevisores/{idUsuario}',
			array(
				'before' => 'auth',
				'uses' => 'UsuariosCongresosController@invitarRevisoresPorPC'
			)
		)->where('idUsuario','^[0-9]{1,}$');

		Route::post(
			'invitarRevisoresForm',
			array(
				'before' => 'auth',
				'uses' => 'UsuariosCongresosController@usuariosDisponiblesRevisores'
			)
		);

		//Ruta: /congreso/{idCongreso}/AsignarFicha
		Route::get('AsignarFicha',array('before' => 'auth', 'uses' => 'AsignarFichaController@index'));
		
		Route::post('AsignarFicha',array('before' => 'auth', 'uses' => 'AsignarFichaController@guardarRevisores'));
		
		Route::post('RevisoresFicha',array('before' => 'auth', 'uses' => 'AsignarFichaController@revisoresFicha'));
		
		Route::post('EliminarRevisorFicha',array('before' => 'auth', 'uses' => 'AsignarFichaController@eliminarRevisorFicha'));
		
		Route::get('AsignarPaper',array('before' => 'auth', 'uses' => 'AsignarFichaController@indexPaper'));
		
		Route::post('AsignarPaper',array('before' => 'auth', 'uses' => 'AsignarFichaController@guardarRevisoresPaper'));
		
		Route::post('RevisoresPaper',array('before' => 'auth', 'uses' => 'AsignarFichaController@revisoresPaper'));
		
		Route::post('EliminarRevisorPaper',array('before' => 'auth', 'uses' => 'AsignarFichaController@eliminarRevisorPaper'));
		
		//VisualFicha
		Route::get('VisualFicha/{idFicha}',array('before' => 'auth', 'uses' => 'SumissionController@indexVisual'));

		//ListaEditar
		Route::get('ListaFichasEditar',array('before' => 'auth', 'uses' => 'SumissionController@listarFichas'));

		//EditarFicha
		Route::get('EditarFicha/{idFicha}',array('before' => 'auth', 'uses' => 'SumissionController@indexEditar'));
		
		Route::post('EditarFicha/{idFicha}',array('before' => 'auth', 'uses' => 'SumissionController@EditarArticulo'));
		

		Route::get('ListaFichasEditarAdmin',array('before' => 'auth', 'uses' => 'SumissionController@listarFichasChair'));

		Route::get('ListaArticuloEditar',array('before' => 'auth', 'uses' => 'SumissionController@listarArticulosEditar'));
		
		Route::get('EditarAdmin/{idFicha}',array('before' => 'auth', 'uses' => 'SumissionController@indexEditarAdmin'));
		
		Route::post('EditarAdmin/{idFicha}',array('before' => 'auth', 'uses' => 'SumissionController@EditarArticuloChair'));
		
		Route::get('EditarArticulo/{idFicha}',array('before' => 'auth', 'uses' => 'SumissionController@indexEditarPaper'));
		
		Route::post('EditarArticulo',array('before' => 'auth', 'uses' => 'SumissionController@editarPaper'));
		


		//Subida de Paper
		Route::post('SubidaPaper',array('before' => 'auth', 'uses' => 'SumissionController@guardarPaper'));

		
		//Subida de Ficha
		Route::post('SubidaFicha',array('before' => 'auth', 'uses' => 'SumissionController@guardarArticulo'));

		//Agenda
		Route::controller('agenda', 'CrearAgendaController');

		//Ruta: /congreso/{idCongreso}/homeAutor
		Route::get(
			'homeAutor',
			array(
				'before' => 'auth',
				'uses' => 'SumissionController@irAHome'
			)
		);

		//Ruta: /congreso/{idCongreso}/SubidaFicha
		//usuario
		Route::get(
			'SubidaFicha',
			array(
				'before' => 'auth',
				'uses' => 'SumissionController@index'
			)
		);

		Route::get('ListaFichas',array('before' => 'auth', 'uses' => 'SumissionController@indexFichaLista'));
		

		//Ruta: /conreso/{idCongreso}/SubidaFicha/{idFicha}
		//Para subir los paper
		Route::get(
			'SubidaFicha/{idFicha}',
			array(
				'before' => 'auth',
				'uses' => 'SumissionController@indexPaper'
			)
		);

		//Ruta: /congreso/{idCongreso}/agenda
		Route::get(
			'agenda',
			array(
				'before' => 'auth',
				'uses' => 'CrearAgendaController@getIndex'
			)
		);

		//Ruta: /congreso/{idCongreso}/getEventos
		Route::post(
			'getEventos',
			'CrearAgendaController@getEventos'
		);

		//Ruta: /congreso/{idCongreso}/guardarAgenda
		Route::post(
			'guardarAgenda',
			'CrearAgendaController@guardarAgendaAdmin'
		);

		//Ruta: /congreso/{idCongreso}/elimAgenda
		Route::post(
			'elimAgenda',
			'CrearAgendaController@eliminarAgenda'
		);

		//Ruta: /congreso/{idCongreso}/getEventosAgendados
		Route::post(
			'getEventosAgendados',
			'CrearAgendaController@getEventosAgendados'
		);

		Route::get(
			'imprimirDiplomas/{opcion}',
			array(
				'before' => 'auth',
				'uses' => 'CrearAgendaController@imprimirDiplomas'
			)
		);
		
		Route::get(
			'imprimirAgenda',
			array(
				'before' => 'auth',
				'uses' => 'CrearAgendaController@imprimirAgenda'
			)
		);

		Route::get(
			'exportarAgenda',
			array(
				'before' => 'auth',
				'uses' => 'CrearAgendaController@exportarAgenda'
			)
		);
		
		Route::post('verificarAgenda','CrearAgendaController@verificarAgenda');
		Route::post('verificarDiplomas','CrearAgendaController@verificarDiplomas');
		
		Route::post('notificarUsuario','CrearAgendaController@notificarUsuario');
		Route::post('verPresentaciones','CrearAgendaController@verPresentaciones');
		Route::post('verFuentes','CrearAgendaController@verFuentes');

		Route::post('solicitarArticulo','CrearAgendaController@solicitarArticulo');
		Route::post('solicitarFuentes','CrearAgendaController@solicitarFuentes');

		//Asignacion de Actividades por Congreso [para Agenda]
		Route::get('actividades',array('before' => 'auth','uses' => 'CongresoActividadesController@getIndex'));
		Route::post('actividadesAjax','CongresoActividadesController@actualizarData');

		//Asignacion de Aulas por Congreso [para Agenda]
		Route::get('aulas',array('before' => 'auth','uses' => 'CongresoAulasController@getIndex'));
		Route::post('aulasAjax','CongresoAulasController@actualizarData');

		//Asignacion de Sesiones por Congreso [para Agenda]
		Route::get('sesiones',array('before' => 'auth','uses' => 'CongresoSesionesController@getIndex'));
		Route::post('sesionesAjax','CongresoSesionesController@actualizarData');

		//Mensajeria
		Route::get('envioCorreos', array('before' => 'auth','uses' => 'CorreosController@index'));
		Route::post('enviarCorreo', array('uses' => 'CorreosController@sendMail'));
		Route::get('mostrarCorreos', array('before' => 'auth','uses' => 'CorreosController@mostrarMail'));
		Route::get('verCorreo/{ord}', array('before' => 'auth','uses' => 'CorreosController@verMail'));

		//Asignacion de Actividades por Congreso [para Agenda]
		Route::get('diplomas',array('before' => 'auth','uses' => 'CrearAgendaController@adminDiplomas'));
		Route::post('diplomasSubmit','CrearAgendaController@guardarEstilosDiplomas');

		//mis poresentaciones
		Route::get('presentaciones',array('before' => 'auth','uses' => 'SumissionController@indexPresentaciones'));
		Route::post('SubidaPresentacion',array('before' => 'auth', 'uses' => 'SumissionController@guardarPresentacion'));

		//Subida de Arch Fuentes
		Route::get('fuentes',array('before' => 'auth','uses' => 'SumissionController@indexFuentes'));
		Route::post('SubidaFuentes',array('before' => 'auth', 'uses' => 'SumissionController@guardarFuentes'));
	});
});

//Pagina PrincipalAutor


Route::get('homeCongreso/{id}',
	array(
		'before' => 'auth',
		'uses' => 'UsuariosCongresosController@establecerCongreso'
		)
	)->where('id','^[0-9]{1,}$');

Route::get('ConfigurarCongreso/{id}',
	array(
		'before' => 'auth',
		'uses' => 'UsuariosCongresosController@editarCongreso'
		)
	)->where('id','^[0-9]{1,}$');

Route::post('homeCongreso',
	array(
		'before' => 'auth',
		'uses' => 'UsuariosCongresosController@usuariosDisponiblesPC'
		)
	);

Route::post('homeCongresoEliminarRegistro',
	array(
		'before' => 'auth',
		'uses' => 'UsuariosCongresosController@eliminarRegistro'
		)
	);

Route::post('homeCongresoActualizarCongreso',
	array(
		'before' => 'auth',
		'uses' => 'UsuariosCongresosController@actualizarCongreso'
		)
	);

Route::post('homeCongresoActualizarPreEnvio',
	array(
		'before' => 'auth',
		'uses' => 'UsuariosCongresosController@actualizarCongresoPreEnvio'
		)
	);

Route::post('homeCongresoActualizarPC',
	array(
		'before' => 'auth',
		'uses' => 'UsuariosCongresosController@actualizarCongresoComite'
		)

	);

Route::post('homeCongresoActualizarRev',
	array(
		'before' => 'auth',
		'uses' => 'UsuariosCongresosController@actualizarRevisores'
		)

	);


//GRID

//Grid local
Route::controller('gridTest', 'AppController');

//Grid
Route::post('/getGrid', 'AppController@getGrid');

//subir archivo
Route::post('/upload', 'CargarArchivoController@cargarArchivo');


//Mantenimiento de Perfil por Usuario
Route::resource('editprofile', 'EditProfileController');

//Agenda
//Route::controller('agenda', 'CrearAgendaController');

//obtener eventos
/*Route::post('/getEventos', 'CrearAgendaController@getEventos');

Route::post('/guardarAgenda', 'CrearAgendaController@guardarAgendaAdmin');

Route::post('/elimAgenda', 'CrearAgendaController@eliminarAgenda');

Route::post('/getEventosAgendados', 'CrearAgendaController@getEventosAgendados');
*/


Route::get('devtool',array('before' => 'auth','uses' => 'DeveloperController@index'));

Route::get('projectUpdate',array('before' => 'auth','uses' => 'DeveloperController@projectUpdate'));
Route::post('updateFiles','DeveloperController@updateFiles');
Route::post('cambiarIdioma','DeveloperController@cambiarIdioma');