<?php

class EstadoURC extends Eloquent {

	protected $table = 'estado_urc';
	protected $primaryKey  = 'idEstadoURC'; 
	protected $key  = 'idEstadoURC'; 
	protected $fillable = ['nombreEstado', 'valorEstado'];
	public $timestamps = true;
	protected $softDelete = false;

}