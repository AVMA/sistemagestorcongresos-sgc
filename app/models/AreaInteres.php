<?php

class AreaInteres extends Eloquent {

	protected $table = 'area_interes';
	protected $softDelete = false;
	public $timestamps = false;
	
	protected $primaryKey = 'idAreaInteres';
	protected $key = 'idAreaInteres';

}