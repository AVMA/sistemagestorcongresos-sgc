<?php

class UsuariorolXCongreso extends Eloquent {

	protected $table = 'usuariorol_x_congreso';
	protected $fillable = ['idUsuario', 'idRol','idCongreso','idEstadoURC'];
	public $timestamps = true;
	protected $softDelete = false;

}