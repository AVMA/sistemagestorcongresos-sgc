<?php

class ExtensionDocumento extends Eloquent {

	protected $table = 'extension_documento';
	public $timestamps = false;
	protected $softDelete = false;
	protected $primaryKey = 'idExtensionDocumento';
	protected $key = 'idExtensionDocumento';

}