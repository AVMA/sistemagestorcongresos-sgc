<?php
//arodriguez.05.19
// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class DetalleCongresoTableSeeder extends Seeder 
{
	public function run()
	{
		DB::table('detalle_congreso')->delete();
		$faker = Faker::create('es_MX');
		$faker->seed(1234);

		/*
			DetalleCongreso::create
			([
				'banner' => 'con3.png',
				'longitudMaxResumenPaper' => 1000,
				//'maxChairVerSumision' => rand(1,50),
				//'numFaxAutor' => $faker->word,
				'dirAutor' => $faker->word,
				'nomRevisorVisible' => true,
				'tamMaxArchivo' =>3,
				'rutaPlantillaDiploma' => $faker->word,
				'idCongreso' => 1
			]);


		DetalleCongreso::create
			([
				'banner' => 'convacio.png',
				'longitudMaxResumenPaper' => 1000,
				//'maxChairVerSumision' => rand(1,50),
				//'numFaxAutor' => $faker->word,
				'dirAutor' => $faker->word,
				'nomRevisorVisible' => true,
				'tamMaxArchivo' => 2,
				'rutaPlantillaDiploma' => $faker->word,
				'idCongreso' => 2
			]);			
		
		DetalleCongreso::create
			([
				'banner' => 'con1.png',
				'longitudMaxResumenPaper' => 500,
				//'maxChairVerSumision' => rand(1,50),
				//'numFaxAutor' => $faker->word,
				'dirAutor' => 'direccion',
				'nomRevisorVisible' => true,
				'tamMaxArchivo' => 4,
				'rutaPlantillaDiploma' => "",
				'idCongreso' => 3
			]);

			*/
	}

}
