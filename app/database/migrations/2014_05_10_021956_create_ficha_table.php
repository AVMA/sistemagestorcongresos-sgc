<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFichaTable extends Migration {

	public function up()
	{
		Schema::create('ficha', function(Blueprint $table) {
			$table->increments('idFicha');
			$table->string('organizacion');
			$table->string('pais');
			$table->timestamps();
			$table->string('tituloPaper');
			$table->string('resumenPaper');
			$table->string('palabrasClaves');
			$table->string('rutaFicha')->nullable();
			$table->integer('idDetalleCongreso')->unsigned();
			$table->integer('idEstadoFicha')->unsigned();
			$table->integer('idTematica')->unsigned();
			$table->integer('idCategoria')->unsigned();
			$table->boolean('agendado'); //arodriguez

		});
	}

	public function down()
	{
		Schema::drop('ficha');
	}
}