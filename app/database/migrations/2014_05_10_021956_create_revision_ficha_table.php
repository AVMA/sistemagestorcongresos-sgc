<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRevisionFichaTable extends Migration {

	public function up()
	{
		Schema::create('revision_ficha', function(Blueprint $table) {
			$table->increments('idRevisionFicha');
			$table->integer('idUsuario')->unsigned();
			$table->integer('idFicha')->unsigned();
			$table->string('comentarioFicha');
			$table->timestamps();
			$table->unique(array('idUsuario', 'idFicha'));
		});
	}

	public function down()
	{
		Schema::drop('revision_ficha');
	}
}