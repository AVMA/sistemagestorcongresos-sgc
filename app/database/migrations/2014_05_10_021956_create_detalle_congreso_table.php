<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetalleCongresoTable extends Migration {

	public function up()
	{
		Schema::create('detalle_congreso', function(Blueprint $table) {
			$table->increments('idDetalleCongreso');
			$table->string('banner'); //El admin debera poner la imagen por defecto al aprobar Imagenes/conconvacio.png
			$table->integer('longitudMaxResumenPaper')->default(500);
			$table->integer('maxChairVerSumision')->nullable();
			$table->integer('numFaxAutor')->nullable();
			$table->string('dirAutor')->nullable();
			$table->boolean('nomRevisorVisible')->default(1);
			$table->integer('tamMaxArchivo')->default(1);
			$table->string('rutaPlantillaDiploma')->nullable();
			$table->string('rutaPlantillaDiploma2')->nullable();
			$table->string('rutaPlantillaDiploma3')->nullable();
			$table->integer('idCongreso')->unsigned();
			$table->timestamps();
			$table->unique('idCongreso');
		});
	}

	public function down()
	{
		Schema::drop('detalle_congreso');
	}
}