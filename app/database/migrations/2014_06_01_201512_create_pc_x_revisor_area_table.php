<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePcXRevisorAreaTable extends Migration {

	public function up()
	{
		Schema::create('pc_x_revisor_area', function(Blueprint $table) {
			$table->increments('idPcRevisorArea');
			$table->integer('idPcRevisor')->unsigned();
			$table->integer('idTematica')->unsigned();
			$table->integer('idUsuario')->unsigned();
			$table->timestamps();
			$table->unique(array('idPcRevisor', 'idTematica', 'idUsuario'));
		});
	}

	public function down()
	{
		Schema::drop('pc_x_revisor_area');
	}
}