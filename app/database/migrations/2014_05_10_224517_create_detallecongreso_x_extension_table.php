<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallecongresoXExtensionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detallecongreso_x_extension', function(Blueprint $table) {
			$table->increments('idDetalleCongresoXExtension');
			$table->integer('idDetalleCongreso')->unsigned();
			$table->integer('idExtensionDocumento')->unsigned();
			$table->timestamps();
			$table->unique(array('idDetalleCongreso', 'idExtensionDocumento'),'detalleCongreso_ExtensionDOcumento_unique');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detallecongreso_x_extension');
	}

}
