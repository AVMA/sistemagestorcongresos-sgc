<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetallecongresoXCategoriaTable extends Migration {

	public function up()
	{
		Schema::create('detallecongreso_x_categoria', function(Blueprint $table) {
			$table->increments('idDetalleCongresoXCategoria');
			$table->integer('idDetalleCongreso')->unsigned();
			$table->integer('idCategoria')->unsigned();
			$table->timestamps();
			$table->unique(array('idDetalleCongreso', 'idCategoria'));
		});
	}

	public function down()
	{
		Schema::drop('detallecongreso_x_categoria');
	}
}