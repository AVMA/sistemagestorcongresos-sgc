<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "La Contraseña debe de contener mas de 7 caractéres y ser igual a la confirmación.",

	"user" => "No se ha encontrado un usuario asociado a ese correo.",

	"token" => "El token proporiconado es invalido.",

	"sent" => "Se ha enviado un correo con las instrucciones necesarias<br />para reiniciar su contraseña!",

);
