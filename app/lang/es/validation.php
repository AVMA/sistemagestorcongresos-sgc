<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "El :attribute debe ser aceptado.",
	"active_url"           => "El :attribute debe de ser una URL valida.",
	"after"                => "El :attribute debe ser despues de :date.",
	"alpha"                => "El :attribute solo puede contener caracteres alfabeticos.",
	"alpha_dash"           => "El :attribute solo puede contener letras, numeros y guiones.",
	"alpha_num"            => "El :attribute solo puede contener letras y numeros.",
	"array"                => "El :attribute tiene que ser un array.",
	"before"               => "El :attribute debe ser una fecha antes que :date.",
	"between"              => array(
		"numeric" => "El :attribute debe estar entre :min y :max.",
		"file"    => "El :attribute debe estar entre :min y :max kilobytes.",
		"string"  => "El :attribute debe de contener entre :min y :max caracteres.",
		"array"   => "El :attribute debe de tener entre :min y :max items.",
	),
	"confirmed"            => "El :attribute de confirmación no coincide.",
	"date"                 => "El :attribute no es una fecha valida.",
	"date_format"          => "El :attribute no cumple con el formato :format.",
	"different"            => "El :attribute y :other deben ser diferentes.",
	"digits"               => "El :attribute debe de tener :digits digitos.",
	"digits_between"       => "El :attribute debe de tener entre :min y :max digitos.",
	"email"                => "El :attribute debe de ser un correo valido.",
	"exists"               => "El :attribute seleccionado es invalido.",
	"image"                => "El :attribute debe de ser una imagen.",
	"in"                   => "El :attribute es inválido.",
	"integer"              => "El :attribute debe de ser un entero.",
	"ip"                   => "El :attribute debe de ser una IP valida.",
	"max"                  => array(
		"numeric" => "El :attribute debe ser menor a :max.",
		"file"    => "El :attribute debe ser menor a :max kilobytes.",
		"string"  => "El :attribute debe ser menor a :max caracteres.",
		"array"   => "El :attribute debe contener menos de :max items.",
	),
	"mimes"                => "El :attribute debe de ser del tipo: :values.",
	"min"                  => array(
		"numeric" => "El :attribute debe ser mayor a :min.",
		"file"    => "El :attribute debe ser mayor a :min kilobytes.",
		"string"  => "El :attribute debe ser mayor a :min caracteres.",
		"array"   => "El :attribute debe de contener mas de :min items.",
	),
	"not_in"               => "El :attribute seleccionado es invalido.",
	"numeric"              => "El :attribute debe ser un numero.",
	"regex"                => "El  formato de :attribute es invalido.",
	"required"             => "El campo :attribute es requerido.",
	"required_if"          => "El campo :attribute cuando el campo :other es :value.",
	"required_with"        => "El campo :attribute es requerido cuando :values esta presente.",
	"required_with_all"    => "El campo :attribute es requerido cuando :values esta presente.",
	"required_without"     => "El campo :attribute es requerido cuando :values no esa presente.",
	"required_without_all" => "El campo :attribute es requerido cuando ningun :values este presente.",
	"same"                 => "El :attribute y :other deben ser iguales.",
	"size"                 => array(
		"numeric" => "El :attribute debe de tener :size.",
		"file"    => "El :attribute debe de tener :size kilobytes.",
		"string"  => "El :attribute debe de tener :size caracteres.",
		"array"   => "El :attribute debe contener :size items.",
	),
	"unique"               => "El :attribute ya esta registrado.",
	"url"                  => "El :attribute debe de ser una url valida.",
	"recaptcha" => 'Se ha fallado la revision del ReCaptcha.',

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
